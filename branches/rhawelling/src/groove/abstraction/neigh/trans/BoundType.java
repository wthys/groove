package groove.abstraction.neigh.trans;

/** Types of equations and variables. */
enum BoundType {
    UB, LB
}