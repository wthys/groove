<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="addToMerged-1" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n137840"/>
        <node id="n137842"/>
        <node id="n137841"/>
        <node id="n137845"/>
        <node id="n137843"/>
        <node id="n137844"/>
        <edge from="n137842" to="n137842">
            <attr name="label">
                <string>rem:</string>
            </attr>
        </edge>
        <edge from="n137843" to="n137843">
            <attr name="label">
                <string>rem:</string>
            </attr>
        </edge>
        <edge from="n137844" to="n137844">
            <attr name="label">
                <string>rem:</string>
            </attr>
        </edge>
        <edge from="n137843" to="n137843">
            <attr name="label">
                <string>trigger is unchanged</string>
            </attr>
        </edge>
        <edge from="n137843" to="n137841">
            <attr name="label">
                <string>trigger</string>
            </attr>
        </edge>
        <edge from="n137844" to="n137845">
            <attr name="label">
                <string>merged</string>
            </attr>
        </edge>
        <edge from="n137845" to="n137845">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n137841" to="n137841">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n137844" to="n137844">
            <attr name="label">
                <string>merged nodes</string>
            </attr>
        </edge>
        <edge from="n137840" to="n137845">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n137840" to="n137840">
            <attr name="label">
                <string>C</string>
            </attr>
        </edge>
        <edge from="n137842" to="n137842">
            <attr name="label">
                <string>new node (with two edges to merged node)</string>
            </attr>
        </edge>
        <edge from="n137840" to="n137845">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n137842" to="n137840">
            <attr name="label">
                <string>new</string>
            </attr>
        </edge>
    </graph>
</gxl>
