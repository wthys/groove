<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="mergeErased-1" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n0"/>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>rem:</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>nothing left</string>
            </attr>
        </edge>
    </graph>
</gxl>
