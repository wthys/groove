<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n147"/>
        <node id="n149"/>
        <node id="n148"/>
        <node id="n146"/>
        <node id="n151"/>
        <node id="n150"/>
        <edge from="n151" to="n150">
            <attr name="label">
                <string>on</string>
            </attr>
        </edge>
        <edge from="n150" to="n150">
            <attr name="label">
                <string>bank</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>goat</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>wolf</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>bank</string>
            </attr>
        </edge>
        <edge from="n146" to="n150">
            <attr name="label">
                <string>on</string>
            </attr>
        </edge>
        <edge from="n146" to="n151">
            <attr name="label">
                <string>likes</string>
            </attr>
        </edge>
        <edge from="n149" to="n150">
            <attr name="label">
                <string>moored</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n151" to="n151">
            <attr name="label">
                <string>cabbage</string>
            </attr>
        </edge>
        <edge from="n148" to="n146">
            <attr name="label">
                <string>likes</string>
            </attr>
        </edge>
        <edge from="n150" to="n150">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n148" to="n150">
            <attr name="label">
                <string>on</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>boat</string>
            </attr>
        </edge>
    </graph>
</gxl>
