<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n33"/>
        <node id="n21"/>
        <node id="n27"/>
        <node id="n28"/>
        <node id="n32"/>
        <node id="n39"/>
        <node id="n38"/>
        <node id="n22"/>
        <node id="n20"/>
        <node id="n41"/>
        <node id="n35"/>
        <node id="n34"/>
        <node id="n25"/>
        <node id="n37"/>
        <node id="n36"/>
        <node id="n26"/>
        <node id="n29"/>
        <node id="n42"/>
        <node id="n30"/>
        <node id="n31"/>
        <node id="n23"/>
        <node id="n24"/>
        <node id="n40"/>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n20" to="n31">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n29" to="n32">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n39" to="n34">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>10</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n32" to="n36">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n24" to="n31">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n27" to="n41">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>15</string>
            </attr>
        </edge>
        <edge from="n22" to="n35">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>-1</string>
            </attr>
        </edge>
        <edge from="n40" to="n21">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n24" to="n25">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n40" to="n25">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n33" to="n21">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge from="n41" to="n42">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n24" to="n21">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n20" to="n21">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>-20</string>
            </attr>
        </edge>
        <edge from="n39" to="n29">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n33" to="n28">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n25" to="n39">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n22" to="n27">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n29" to="n38">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n27" to="n37">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n32" to="n30">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n40" to="n36">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n33" to="n25">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n25" to="n26">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n21" to="n25">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>11</string>
            </attr>
        </edge>
        <edge from="n20" to="n25">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n30" to="n22">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n30" to="n23">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
    </graph>
</gxl>
