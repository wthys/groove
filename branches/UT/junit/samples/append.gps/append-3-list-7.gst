<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n143"/>
        <node id="n129"/>
        <node id="n140"/>
        <node id="n125"/>
        <node id="n137"/>
        <node id="n126"/>
        <node id="n133"/>
        <node id="n134"/>
        <node id="n138"/>
        <node id="n136"/>
        <node id="n135"/>
        <node id="n139"/>
        <node id="n127"/>
        <node id="n132"/>
        <node id="n131"/>
        <node id="n144"/>
        <node id="n141"/>
        <node id="n128"/>
        <node id="n142"/>
        <node id="n130"/>
        <edge from="n126" to="n134">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge from="n135" to="n141">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n138" to="n133">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>11</string>
            </attr>
        </edge>
        <edge from="n138" to="n138">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n138" to="n127">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n143" to="n129">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n127" to="n131">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n134" to="n144">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n143" to="n127">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n143" to="n142">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n144" to="n125">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n134" to="n140">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n142" to="n142">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n132" to="n126">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n125" to="n125">
            <attr name="label">
                <string>-1</string>
            </attr>
        </edge>
        <edge from="n144" to="n135">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n129" to="n127">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge from="n132" to="n139">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>15</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n130" to="n127">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n130" to="n129">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n128" to="n128">
            <attr name="label">
                <string>10</string>
            </attr>
        </edge>
        <edge from="n127" to="n137">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n139" to="n139">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge from="n130" to="n142">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n137" to="n132">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n126" to="n128">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n138" to="n138">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n137" to="n136">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n138" to="n129">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
    </graph>
</gxl>
