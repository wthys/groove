<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="newGraph" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n57677"/>
        <node id="n57678"/>
        <node id="n57679"/>
        <node id="n57680"/>
        <node id="n57681"/>
        <node id="n57682"/>
        <node id="n57683"/>
        <node id="n57684"/>
        <node id="n57685"/>
        <node id="n57686"/>
        <node id="n57687"/>
        <node id="n57688"/>
        <node id="n57689"/>
        <node id="n57690"/>
        <node id="n57691"/>
        <node id="n57692"/>
        <node id="n57693"/>
        <edge from="n57691" to="n57681">
            <attr name="label">
                <string>d</string>
            </attr>
        </edge>
        <edge from="n57682" to="n57686">
            <attr name="label">
                <string>[a[x1^x2],[a[x1^!x2],a[!x1^x2]]</string>
            </attr>
        </edge>
        <edge from="n57688" to="n57692">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n57689" to="n57677">
            <attr name="label">
                <string>a[x2][]</string>
            </attr>
        </edge>
        <edge from="n57688" to="n57687">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge from="n57680" to="n57693">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n57686" to="n57683">
            <attr name="label">
                <string>d</string>
            </attr>
        </edge>
        <edge from="n57682" to="n57679">
            <attr name="label">
                <string>a[!x1^x2]</string>
            </attr>
        </edge>
        <edge from="n57682" to="n57684">
            <attr name="label">
                <string>a[x1^!x2]</string>
            </attr>
        </edge>
        <edge from="n57684" to="n57685">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n57677" to="n57690">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge from="n57682" to="n57688">
            <attr name="label">
                <string>a[x1^x2][]</string>
            </attr>
        </edge>
        <edge from="n57689" to="n57680">
            <attr name="label">
                <string>a[x1][]</string>
            </attr>
        </edge>
        <edge from="n57689" to="n57691">
            <attr name="label">
                <string>[a[x1],a[x2]]</string>
            </attr>
        </edge>
        <edge from="n57679" to="n57678">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
    </graph>
</gxl>
