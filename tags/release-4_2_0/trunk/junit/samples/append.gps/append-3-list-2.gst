<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="append-3-list-2">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n119"/>
        <node id="n116"/>
        <node id="n123"/>
        <node id="n117"/>
        <node id="n115"/>
        <node id="n124"/>
        <node id="n118"/>
        <node id="n121"/>
        <node id="n110"/>
        <node id="n111"/>
        <edge to="n124" from="n121">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge to="n119" from="n119">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge to="n121" from="n121">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge to="n123" from="n123">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge to="n115" from="n115">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge to="n115" from="n123">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge to="n123" from="n123">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge to="n111" from="n111">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge to="n115" from="n111">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge to="n117" from="n111">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge to="n121" from="n121">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge to="n124" from="n123">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge to="n116" from="n116">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge to="n117" from="n121">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge to="n118" from="n124">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n110" from="n121">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge to="n124" from="n117">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge to="n110" from="n110">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge to="n111" from="n111">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge to="n116" from="n118">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n117" from="n117">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge to="n124" from="n111">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge to="n117" from="n123">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge to="n119" from="n124">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
    </graph>
</gxl>
