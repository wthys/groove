<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start4" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n719"/>
        <node id="n722"/>
        <node id="n723"/>
        <node id="n725"/>
        <node id="n720"/>
        <node id="n724"/>
        <node id="n721"/>
        <edge from="n723" to="n719">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n720" to="n719">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n721" to="n721">
            <attr name="label">
                <string>h</string>
            </attr>
        </edge>
        <edge from="n724" to="n724">
            <attr name="label">
                <string>i</string>
            </attr>
        </edge>
        <edge from="n720" to="n723">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n722" to="n723">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n720" to="n720">
            <attr name="label">
                <string>g</string>
            </attr>
        </edge>
        <edge from="n724" to="n725">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n719" to="n719">
            <attr name="label">
                <string>g</string>
            </attr>
        </edge>
        <edge from="n722" to="n722">
            <attr name="label">
                <string>g</string>
            </attr>
        </edge>
        <edge from="n723" to="n723">
            <attr name="label">
                <string>g</string>
            </attr>
        </edge>
        <edge from="n725" to="n725">
            <attr name="label">
                <string>i</string>
            </attr>
        </edge>
    </graph>
</gxl>
