<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n4"/>
        <node id="n5"/>
        <node id="n6"/>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
    </graph>
</gxl>
