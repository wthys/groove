<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n10674"/>
        <node id="n10671"/>
        <node id="n10668"/>
        <node id="n10670"/>
        <node id="n10669"/>
        <node id="n10672"/>
        <node id="n10673"/>
        <node id="n10666"/>
        <node id="n10667"/>
        <edge from="n10668" to="n10671">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n10666" to="n10666">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n10668" to="n10672">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n10669" to="n10669">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n10670" to="n10670">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge from="n10667" to="n10667">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n10673" to="n10668">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n10673" to="n10673">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n10672" to="n10672">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n10669" to="n10674">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n10669" to="n10669">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n10673" to="n10674">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n10673" to="n10666">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n10673" to="n10673">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n10669" to="n10670">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n10669" to="n10668">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n10674" to="n10674">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge from="n10671" to="n10667">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n10674" to="n10668">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
    </graph>
</gxl>
