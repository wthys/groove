<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="append-2-list-6">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n103"/>
        <node id="n93"/>
        <node id="n1"/>
        <node id="n95"/>
        <node id="n94"/>
        <node id="n0"/>
        <node id="n90"/>
        <node id="n99"/>
        <node id="n98"/>
        <node id="n91"/>
        <node id="n101"/>
        <node id="n102"/>
        <node id="n97"/>
        <node id="n100"/>
        <node id="n96"/>
        <node id="n104"/>
        <node id="n92"/>
        <edge to="n101" from="n101">
            <attr name="label">
                <string>6</string>
            </attr>
        </edge>
        <edge to="n100" from="n100">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge to="n100" from="n96">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge to="n91" from="n91">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge to="n90" from="n103">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n92" from="n92">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge to="n102" from="n102">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge to="n92" from="n94">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge to="n99" from="n95">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n1" from="n0">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n0" from="n99">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n103" from="n92">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge to="n93" from="n93">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge to="n94" from="n94">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge to="n98" from="n98">
            <attr name="label">
                <string>7</string>
            </attr>
        </edge>
        <edge to="n94" from="n94">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge to="n92" from="n96">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge to="n104" from="n104">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge to="n97" from="n90">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n103" from="n94">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge to="n103" from="n96">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>8</string>
            </attr>
        </edge>
        <edge to="n95" from="n97">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n104" from="n97">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n98" from="n99">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n93" from="n103">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n102" from="n94">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge to="n96" from="n96">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge to="n96" from="n96">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge to="n91" from="n90">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n101" from="n95">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
    </graph>
</gxl>
