<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="shape-build-test-0">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n0"/>
        <node id="n3"/>
        <node id="n8"/>
        <node id="n2"/>
        <node id="n4"/>
        <node id="n1"/>
        <node id="n5"/>
        <node id="n6"/>
        <node id="n7"/>
        <edge to="n5" from="n1">
            <attr name="label">
                <string>v</string>
            </attr>
        </edge>
        <edge to="n6" from="n6">
            <attr name="label">
                <string>type:O</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>type:C</string>
            </attr>
        </edge>
        <edge to="n3" from="n2">
            <attr name="label">
                <string>n</string>
            </attr>
        </edge>
        <edge to="n7" from="n3">
            <attr name="label">
                <string>v</string>
            </attr>
        </edge>
        <edge to="n8" from="n8">
            <attr name="label">
                <string>type:O</string>
            </attr>
        </edge>
        <edge to="n8" from="n4">
            <attr name="label">
                <string>v</string>
            </attr>
        </edge>
        <edge to="n2" from="n1">
            <attr name="label">
                <string>n</string>
            </attr>
        </edge>
        <edge to="n1" from="n0">
            <attr name="label">
                <string>n</string>
            </attr>
        </edge>
        <edge to="n6" from="n2">
            <attr name="label">
                <string>v</string>
            </attr>
        </edge>
        <edge to="n7" from="n7">
            <attr name="label">
                <string>type:O</string>
            </attr>
        </edge>
        <edge to="n4" from="n3">
            <attr name="label">
                <string>n</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>type:L</string>
            </attr>
        </edge>
        <edge to="n2" from="n2">
            <attr name="label">
                <string>type:C</string>
            </attr>
        </edge>
        <edge to="n4" from="n4">
            <attr name="label">
                <string>type:C</string>
            </attr>
        </edge>
        <edge to="n5" from="n5">
            <attr name="label">
                <string>type:O</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>type:C</string>
            </attr>
        </edge>
    </graph>
</gxl>
