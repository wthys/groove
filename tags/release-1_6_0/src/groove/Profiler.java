package groove;

/**
 * Wrapper class for the profiler
 * @see groove.util.Profiler
 * @author J. ter Hove
 */
public class Profiler {
    /**
     * Main method.
     * @param args list of command-line arguments
     */
    static public void main(String[] args) {
        groove.util.Profiler.main(args);
    }
}
