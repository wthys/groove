/* $Id: GraphPredicateOutcome.java,v 1.1.1.2 2007-03-20 10:42:56 kastenberg Exp $ */
package groove.trans;

/**
 * A specialised interface that models the outcome of a graph predicate.
 * @author Arend Rensink
 * @version $Revision $
 */
public interface GraphPredicateOutcome extends GraphTestOutcome<GraphCondition,Matching> {
	// nothing but a specialised interface
}
