/* $Id: GraphConditionOutcome.java,v 1.1.1.2 2007-03-20 10:42:55 kastenberg Exp $ */
package groove.trans;

/**
 * A specialised interface that models the outcome of a graph condition.
 * @author Arend Rensink
 * @version $Revision $
 */
public interface GraphConditionOutcome extends GraphTestOutcome<Matching,GraphCondition> {
	// nothing but a specialised interface
}
