<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n105"/>
        <node id="n93"/>
        <node id="n97"/>
        <node id="n95"/>
        <node id="n100"/>
        <node id="n94"/>
        <node id="n101"/>
        <node id="n102"/>
        <node id="n98"/>
        <node id="n99"/>
        <node id="n104"/>
        <node id="n96"/>
        <node id="n92"/>
        <node id="n91"/>
        <node id="n103"/>
        <edge from="n105" to="n105">
            <attr name="label">
                <string>8</string>
            </attr>
        </edge>
        <edge from="n93" to="n94">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n93" to="n98">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n97" to="n97">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>List</string>
            </attr>
        </edge>
        <edge from="n95" to="n92">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n100" to="n102">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n100" to="n101">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge from="n101" to="n101">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n102" to="n104">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n102" to="n103">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n98" to="n99">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n98" to="n100">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge from="n104" to="n105">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n92" to="n96">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n92" to="n91">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n91" to="n97">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n91" to="n93">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n103" to="n103">
            <attr name="label">
                <string>6</string>
            </attr>
        </edge>
    </graph>
</gxl>
