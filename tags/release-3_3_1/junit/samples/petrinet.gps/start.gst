<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n0"/>
        <node id="n1"/>
        <node id="n4"/>
        <node id="n9"/>
        <node id="n5"/>
        <node id="n6"/>
        <node id="n2"/>
        <node id="n3"/>
        <node id="n11"/>
        <node id="n12"/>
        <node id="n8"/>
        <node id="n10"/>
        <node id="n7"/>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n8" to="n1">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n4" to="n11">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n7" to="n9">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n11" to="n0">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n9" to="n3">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n10" to="n6">
            <attr name="label">
                <string>mark</string>
            </attr>
        </edge>
        <edge from="n3" to="n4">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>token</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n1" to="n2">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n4" to="n8">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>token</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n2" to="n12">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n12" to="n10">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n3" to="n5">
            <attr name="label">
                <string>mark</string>
            </attr>
        </edge>
        <edge from="n0" to="n7">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n10" to="n4">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
    </graph>
</gxl>
