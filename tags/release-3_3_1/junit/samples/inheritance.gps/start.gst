<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n293"/>
        <node id="n294"/>
        <node id="n296"/>
        <node id="n295"/>
        <node id="n292"/>
        <node id="n291"/>
        <node id="n297"/>
        <edge from="n296" to="n296">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge from="n291" to="n296">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n295" to="n295">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge from="n291" to="n293">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n292" to="n296">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n297" to="n297">
            <attr name="label">
                <string>type:C</string>
            </attr>
        </edge>
        <edge from="n291" to="n291">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge from="n292" to="n297">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n291" to="n297">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n295" to="n295">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n294" to="n294">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n297" to="n296">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n292" to="n293">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n292" to="n292">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge from="n294" to="n294">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge from="n297" to="n293">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n293" to="n293">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
    </graph>
</gxl>
