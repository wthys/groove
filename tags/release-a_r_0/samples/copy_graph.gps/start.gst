<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n16828"/>
        <node id="n16829"/>
        <node id="n16830"/>
        <edge from="n16828" to="n16828">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge from="n16829" to="n16829">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n16830" to="n16830">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
    </graph>
</gxl>
