/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2007 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.abstraction.pattern.explore.util;

import groove.abstraction.pattern.lts.MatchResult;
import groove.abstraction.pattern.lts.PatternState;
import groove.lts.MatchApplier;

/**
 * Common interface for match appliers of pattern graph and pattern shape
 * transition systems.
 * 
 * See {@link MatchApplier}.
 */
public interface PatternRuleEventApplier {

    /**
     * Adds a transition to the PGTS, from a given source state and for a given
     * rule event. The event is assumed not to have been explored yet.
     */
    void apply(PatternState source, MatchResult match);

}