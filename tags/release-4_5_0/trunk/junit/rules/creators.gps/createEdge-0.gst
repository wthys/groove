<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="createEdge-0">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n5">
            <attr name="layout">
                <string>317 249 29 46</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>315 182 29 46</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>406 182 28 31</string>
            </attr>
        </node>
        <node id="n1">
            <attr name="layout">
                <string>407 111 28 31</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>485 183 28 31</string>
            </attr>
        </node>
        <node id="n7">
            <attr name="layout">
                <string>484 112 274 31</string>
            </attr>
        </node>
        <node id="n8">
            <attr name="layout">
                <string>410 238 28 31</string>
            </attr>
        </node>
        <node id="n6">
            <attr name="layout">
                <string>52 63 251 31</string>
            </attr>
        </node>
        <node id="n0">
            <attr name="layout">
                <string>313 105 29 46</string>
            </attr>
        </node>
        <edge to="n5" from="n5">
            <attr name="label">
                <string>flag:2</string>
            </attr>
        </edge>
        <edge to="n4" from="n3">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge to="n3" from="n0">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge to="n6" from="n6">
            <attr name="label">
                <string>Edges will be added to these three nodes</string>
            </attr>
        </edge>
        <edge to="n2" from="n2">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n4" from="n8">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge to="n2" from="n6">
            <attr name="label">
                <string>two</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge to="n1" from="n7">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge to="n5" from="n6">
            <attr name="label">
                <string>three</string>
            </attr>
        </edge>
        <edge to="n4" from="n4">
            <attr name="label">
                <string>type:C</string>
            </attr>
        </edge>
        <edge to="n2" from="n2">
            <attr name="label">
                <string>flag:1</string>
            </attr>
        </edge>
        <edge to="n7" from="n7">
            <attr name="label">
                <string>rem:</string>
            </attr>
        </edge>
        <edge to="n8" from="n8">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge to="n5" from="n5">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>flag:0</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n6" from="n6">
            <attr name="label">
                <string>rem:</string>
            </attr>
        </edge>
        <edge to="n7" from="n7">
            <attr name="label">
                <string>This target node does nothave the required C</string>
            </attr>
        </edge>
        <edge to="n0" from="n6">
            <attr name="label">
                <string>one</string>
            </attr>
        </edge>
    </graph>
</gxl>
