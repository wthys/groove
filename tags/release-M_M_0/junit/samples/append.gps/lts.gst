<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="s62"/>
        <node id="s68"/>
        <node id="s75"/>
        <node id="s58"/>
        <node id="s72"/>
        <node id="s67"/>
        <node id="s63"/>
        <node id="s66"/>
        <node id="s76"/>
        <node id="s71"/>
        <node id="s59"/>
        <node id="s64"/>
        <node id="s73"/>
        <node id="s77"/>
        <node id="s65"/>
        <node id="s70"/>
        <node id="s60"/>
        <node id="s74"/>
        <node id="s61"/>
        <node id="s57"/>
        <node id="s69"/>
        <edge from="s63" to="s64">
            <attr name="label">
                <string>&lt;next&gt;</string>
            </attr>
        </edge>
        <edge from="s60" to="s62">
            <attr name="label">
                <string>&lt;next&gt;</string>
            </attr>
        </edge>
        <edge from="s57" to="s58">
            <attr name="label">
                <string>&lt;next&gt;</string>
            </attr>
        </edge>
        <edge from="s67" to="s68">
            <attr name="label">
                <string>&lt;next&gt;</string>
            </attr>
        </edge>
        <edge from="s57" to="s59">
            <attr name="label">
                <string>&lt;next&gt;</string>
            </attr>
        </edge>
        <edge from="s75" to="s76">
            <attr name="label">
                <string>&lt;return&gt;</string>
            </attr>
        </edge>
        <edge from="s58" to="s58">
            <attr name="label">
                <string>open</string>
            </attr>
        </edge>
        <edge from="s72" to="s74">
            <attr name="label">
                <string>&lt;append&gt;</string>
            </attr>
        </edge>
        <edge from="s76" to="s77">
            <attr name="label">
                <string>&lt;return&gt;</string>
            </attr>
        </edge>
        <edge from="s64" to="s66">
            <attr name="label">
                <string>&lt;append&gt;</string>
            </attr>
        </edge>
        <edge from="s62" to="s62">
            <attr name="label">
                <string>open</string>
            </attr>
        </edge>
        <edge from="s65" to="s65">
            <attr name="label">
                <string>open</string>
            </attr>
        </edge>
        <edge from="s68" to="s70">
            <attr name="label">
                <string>&lt;return&gt;</string>
            </attr>
        </edge>
        <edge from="s74" to="s75">
            <attr name="label">
                <string>&lt;return&gt;</string>
            </attr>
        </edge>
        <edge from="s61" to="s61">
            <attr name="label">
                <string>open</string>
            </attr>
        </edge>
        <edge from="s70" to="s73">
            <attr name="label">
                <string>&lt;append&gt;</string>
            </attr>
        </edge>
        <edge from="s63" to="s65">
            <attr name="label">
                <string>&lt;append&gt;</string>
            </attr>
        </edge>
        <edge from="s59" to="s60">
            <attr name="label">
                <string>&lt;next&gt;</string>
            </attr>
        </edge>
        <edge from="s57" to="s57">
            <attr name="label">
                <string>start</string>
            </attr>
        </edge>
        <edge from="s70" to="s72">
            <attr name="label">
                <string>&lt;return&gt;</string>
            </attr>
        </edge>
        <edge from="s64" to="s67">
            <attr name="label">
                <string>&lt;append&gt;</string>
            </attr>
        </edge>
        <edge from="s60" to="s63">
            <attr name="label">
                <string>&lt;next&gt;</string>
            </attr>
        </edge>
        <edge from="s77" to="s77">
            <attr name="label">
                <string>final</string>
            </attr>
        </edge>
        <edge from="s73" to="s73">
            <attr name="label">
                <string>open</string>
            </attr>
        </edge>
        <edge from="s71" to="s71">
            <attr name="label">
                <string>open</string>
            </attr>
        </edge>
        <edge from="s66" to="s66">
            <attr name="label">
                <string>open</string>
            </attr>
        </edge>
        <edge from="s59" to="s61">
            <attr name="label">
                <string>&lt;next&gt;</string>
            </attr>
        </edge>
        <edge from="s67" to="s69">
            <attr name="label">
                <string>&lt;return&gt;</string>
            </attr>
        </edge>
        <edge from="s69" to="s69">
            <attr name="label">
                <string>open</string>
            </attr>
        </edge>
        <edge from="s68" to="s71">
            <attr name="label">
                <string>&lt;append&gt;</string>
            </attr>
        </edge>
    </graph>
</gxl>
