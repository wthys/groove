<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="shape-build-test-1">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n7"/>
        <node id="n2"/>
        <node id="n1"/>
        <node id="n5"/>
        <node id="n0"/>
        <node id="n8"/>
        <node id="n9"/>
        <node id="n12"/>
        <node id="n4"/>
        <node id="n11"/>
        <node id="n6"/>
        <node id="n10"/>
        <node id="n3"/>
        <edge to="n11" from="n11">
            <attr name="label">
                <string>flag:s</string>
            </attr>
        </edge>
        <edge to="n8" from="n8">
            <attr name="label">
                <string>type:LO</string>
            </attr>
        </edge>
        <edge to="n7" from="n7">
            <attr name="label">
                <string>type:LO</string>
            </attr>
        </edge>
        <edge to="n9" from="n11">
            <attr name="label">
                <string>at</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>type:IF</string>
            </attr>
        </edge>
        <edge to="n5" from="n4">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge to="n2" from="n2">
            <attr name="label">
                <string>type:LI</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>type:LI</string>
            </attr>
        </edge>
        <edge to="n6" from="n6">
            <attr name="label">
                <string>type:LO</string>
            </attr>
        </edge>
        <edge to="n3" from="n4">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge to="n5" from="n5">
            <attr name="label">
                <string>type:IF</string>
            </attr>
        </edge>
        <edge to="n1" from="n0">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n5" from="n6">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n9" from="n9">
            <attr name="label">
                <string>type:LO</string>
            </attr>
        </edge>
        <edge to="n6" from="n10">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n10" from="n9">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n8" from="n7">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n7" from="n6">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n4" from="n4">
            <attr name="label">
                <string>type:FW</string>
            </attr>
        </edge>
        <edge to="n9" from="n8">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n12" from="n12">
            <attr name="label">
                <string>flag:u</string>
            </attr>
        </edge>
        <edge to="n3" from="n2">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n10" from="n10">
            <attr name="label">
                <string>type:LO</string>
            </attr>
        </edge>
        <edge to="n12" from="n12">
            <attr name="label">
                <string>type:P</string>
            </attr>
        </edge>
        <edge to="n3" from="n0">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>type:LI</string>
            </attr>
        </edge>
        <edge to="n9" from="n12">
            <attr name="label">
                <string>at</string>
            </attr>
        </edge>
        <edge to="n1" from="n2">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n11" from="n11">
            <attr name="label">
                <string>type:P</string>
            </attr>
        </edge>
    </graph>
</gxl>
