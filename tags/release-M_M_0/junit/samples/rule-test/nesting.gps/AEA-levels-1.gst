<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="AEA-levels-1" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n138732"/>
        <node id="n138730"/>
        <node id="n138728"/>
        <node id="n138727"/>
        <node id="n138729"/>
        <node id="n138726"/>
        <node id="n138731"/>
        <node id="n138725"/>
        <edge from="n138729" to="n138729">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n138726" to="n138728">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge from="n138728" to="n138728">
            <attr name="label">
                <string>C</string>
            </attr>
        </edge>
        <edge from="n138727" to="n138727">
            <attr name="label">
                <string>D</string>
            </attr>
        </edge>
        <edge from="n138726" to="n138726">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n138725" to="n138725">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n138725" to="n138732">
            <attr name="label">
                <string>copy</string>
            </attr>
        </edge>
        <edge from="n138726" to="n138731">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n138729" to="n138731">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n138731" to="n138731">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n138730" to="n138730">
            <attr name="label">
                <string>D</string>
            </attr>
        </edge>
        <edge from="n138731" to="n138730">
            <attr name="label">
                <string>d</string>
            </attr>
        </edge>
        <edge from="n138732" to="n138732">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n138731" to="n138727">
            <attr name="label">
                <string>d</string>
            </attr>
        </edge>
    </graph>
</gxl>
