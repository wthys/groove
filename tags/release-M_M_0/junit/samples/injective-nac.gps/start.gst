<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n59006"/>
        <node id="n59007"/>
        <node id="n59008"/>
        <node id="n59009"/>
        <edge from="n59008" to="n59007">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n59009" to="n59007">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n59006" to="n59006">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n59007" to="n59007">
            <attr name="label">
                <string>C</string>
            </attr>
        </edge>
        <edge from="n59009" to="n59009">
            <attr name="label">
                <string>C</string>
            </attr>
        </edge>
        <edge from="n59008" to="n59006">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n59008" to="n59008">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
    </graph>
</gxl>
