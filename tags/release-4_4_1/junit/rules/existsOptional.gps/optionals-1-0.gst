<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="optionals-1-0">
        <node id="n0">
            <attr name="layout">
                <string>234 87 34 61</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>99 85 34 61</string>
            </attr>
        </node>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>flag:0</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>flag:mark</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>flag:1</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
    </graph>
</gxl>
