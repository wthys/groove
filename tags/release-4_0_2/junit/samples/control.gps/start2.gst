<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start2" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n1612"/>
        <node id="n1613"/>
        <node id="n1614"/>
        <node id="n1615"/>
        <node id="n1616"/>
        <node id="n1617"/>
        <node id="n1618"/>
        <edge from="n1612" to="n1616">
            <attr name="label">
                <string>dest</string>
            </attr>
        </edge>
        <edge from="n1615" to="n1616">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n1614" to="n1614">
            <attr name="label">
                <string>Person</string>
            </attr>
        </edge>
        <edge from="n1617" to="n1616">
            <attr name="label">
                <string>dest</string>
            </attr>
        </edge>
        <edge from="n1617" to="n1613">
            <attr name="label">
                <string>at</string>
            </attr>
        </edge>
        <edge from="n1612" to="n1615">
            <attr name="label">
                <string>at</string>
            </attr>
        </edge>
        <edge from="n1614" to="n1613">
            <attr name="label">
                <string>at</string>
            </attr>
        </edge>
        <edge from="n1617" to="n1617">
            <attr name="label">
                <string>Person</string>
            </attr>
        </edge>
        <edge from="n1616" to="n1613">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n1618" to="n1618">
            <attr name="label">
                <string>Train</string>
            </attr>
        </edge>
        <edge from="n1613" to="n1615">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n1612" to="n1612">
            <attr name="label">
                <string>Person</string>
            </attr>
        </edge>
        <edge from="n1615" to="n1615">
            <attr name="label">
                <string>Station</string>
            </attr>
        </edge>
        <edge from="n1618" to="n1616">
            <attr name="label">
                <string>at</string>
            </attr>
        </edge>
        <edge from="n1616" to="n1616">
            <attr name="label">
                <string>Station</string>
            </attr>
        </edge>
        <edge from="n1614" to="n1615">
            <attr name="label">
                <string>dest</string>
            </attr>
        </edge>
        <edge from="n1613" to="n1613">
            <attr name="label">
                <string>Station</string>
            </attr>
        </edge>
    </graph>
</gxl>
