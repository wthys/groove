<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n12"/>
        <node id="n13"/>
        <node id="n14"/>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n13" to="n13">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
    </graph>
</gxl>
