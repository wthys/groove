<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n59013"/>
        <node id="n59014"/>
        <node id="n59015"/>
        <node id="n59016"/>
        <edge from="n59013" to="n59016">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n59013" to="n59013">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n59014" to="n59014">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n59015" to="n59015">
            <attr name="label">
                <string>C</string>
            </attr>
        </edge>
        <edge from="n59015" to="n59016">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n59014" to="n59016">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
    </graph>
</gxl>
