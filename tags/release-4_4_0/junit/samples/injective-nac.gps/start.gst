<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="start">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n59006">
            <attr name="layout">
                <string>367 298 7 15</string>
            </attr>
        </node>
        <node id="n59008">
            <attr name="layout">
                <string>407 204 8 15</string>
            </attr>
        </node>
        <node id="n59009">
            <attr name="layout">
                <string>566 265 7 15</string>
            </attr>
        </node>
        <node id="n59007">
            <attr name="layout">
                <string>474 274 7 15</string>
            </attr>
        </node>
        <edge to="n59006" from="n59006">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge to="n59007" from="n59008">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge to="n59009" from="n59009">
            <attr name="label">
                <string>C</string>
            </attr>
        </edge>
        <edge to="n59007" from="n59009">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge to="n59007" from="n59007">
            <attr name="label">
                <string>C</string>
            </attr>
        </edge>
        <edge to="n59008" from="n59008">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge to="n59006" from="n59008">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
    </graph>
</gxl>
