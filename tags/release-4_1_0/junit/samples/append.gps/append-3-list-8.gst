<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="append-3-list-8">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n10"/>
        <node id="n18"/>
        <node id="n1"/>
        <node id="n12"/>
        <node id="n9"/>
        <node id="n7"/>
        <node id="n21"/>
        <node id="n4"/>
        <node id="n3"/>
        <node id="n2"/>
        <node id="n17"/>
        <node id="n19"/>
        <node id="n11"/>
        <node id="n6"/>
        <node id="n5"/>
        <node id="n15"/>
        <node id="n14"/>
        <node id="n22"/>
        <node id="n20"/>
        <node id="n16"/>
        <node id="n8"/>
        <node id="n13"/>
        <edge to="n10" from="n10">
            <attr name="label">
                <string>'-20'</string>
            </attr>
        </edge>
        <edge to="n19" from="n19">
            <attr name="label">
                <string>15</string>
            </attr>
        </edge>
        <edge to="n18" from="n18">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge to="n4" from="n17">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge to="n12" from="n12">
            <attr name="label">
                <string>'-1'</string>
            </attr>
        </edge>
        <edge to="n5" from="n13">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n18" from="n18">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge to="n2" from="n8">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n12" from="n11">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n14" from="n5">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n20" from="n17">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge to="n4" from="n3">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge to="n7" from="n15">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n17" from="n17">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge to="n21" from="n21">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge to="n21" from="n18">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge to="n16" from="n5">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n6" from="n6">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge to="n19" from="n15">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n22" from="n9">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n13" from="n3">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge to="n11" from="n9">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n20" from="n20">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge to="n15" from="n11">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge to="n10" from="n7">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n13" from="n4">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge to="n22" from="n22">
            <attr name="label">
                <string>11</string>
            </attr>
        </edge>
        <edge to="n1" from="n13">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n13" from="n18">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge to="n20" from="n3">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge to="n6" from="n14">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge to="n4" from="n18">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge to="n2" from="n2">
            <attr name="label">
                <string>10</string>
            </attr>
        </edge>
        <edge to="n17" from="n17">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge to="n8" from="n14">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n9" from="n8">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge to="n16" from="n16">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge to="n13" from="n17">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge to="n4" from="n4">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
    </graph>
</gxl>
