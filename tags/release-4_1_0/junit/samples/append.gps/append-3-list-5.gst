<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n110"/>
        <node id="n124"/>
        <node id="n111"/>
        <node id="n119"/>
        <node id="n112"/>
        <node id="n115"/>
        <node id="n116"/>
        <node id="n123"/>
        <node id="n118"/>
        <node id="n120"/>
        <node id="n117"/>
        <node id="n121"/>
        <node id="n122"/>
        <node id="n113"/>
        <node id="n114"/>
        <node id="n125"/>
        <edge from="n125" to="n125">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge from="n123" to="n124">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n121" to="n124">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n112" to="n112">
            <attr name="label">
                <string>11</string>
            </attr>
        </edge>
        <edge from="n118" to="n113">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n117" to="n117">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge from="n117" to="n124">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge from="n123" to="n115">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n120" to="n112">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n121" to="n110">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n123" to="n117">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n118" to="n116">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n121" to="n117">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge from="n124" to="n118">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n116" to="n116">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n124" to="n119">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n113" to="n114">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n111" to="n115">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n114" to="n120">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n115" to="n115">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n111" to="n124">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>10</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n113" to="n125">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n111" to="n117">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n114" to="n122">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
    </graph>
</gxl>
