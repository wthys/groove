<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="mergEraserCreator-start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n138043"/>
        <node id="n138042"/>
        <node id="n138041"/>
        <edge from="n138043" to="n138043">
            <attr name="label">
                <string>rem:</string>
            </attr>
        </edge>
        <edge from="n138041" to="n138041">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n138043" to="n138041">
            <attr name="label">
                <string>A-node</string>
            </attr>
        </edge>
        <edge from="n138042" to="n138042">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n138043" to="n138043">
            <attr name="label">
                <string>Two node, which will be matched by both the top level A and the sublevel A</string>
            </attr>
        </edge>
        <edge from="n138043" to="n138042">
            <attr name="label">
                <string>A-node</string>
            </attr>
        </edge>
    </graph>
</gxl>
