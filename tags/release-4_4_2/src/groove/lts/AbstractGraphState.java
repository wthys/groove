/*
 * GROOVE: GRaphs for Object Oriented VErification Copyright 2003--2007
 * University of Twente
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * $Id: AbstractGraphState.java,v 1.17 2008-02-20 09:25:29 kastenberg Exp $
 */
package groove.lts;

import groove.control.CtrlSchedule;
import groove.control.CtrlState;
import groove.graph.Element;
import groove.graph.Graph;
import groove.trans.HostElement;
import groove.trans.HostNode;
import groove.trans.RuleEvent;
import groove.trans.SystemRecord;
import groove.util.AbstractCacheHolder;
import groove.util.CacheReference;
import groove.util.TransformIterator;
import groove.util.TransformSet;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Combination of graph and node functionality, used to store the state of a
 * graph transition system.
 * 
 * @author Arend Rensink
 * @version $Revision$ $Date: 2008-02-20 09:25:29 $
 */
abstract public class AbstractGraphState extends
        AbstractCacheHolder<StateCache> implements GraphState {
    /**
     * Constructs a an abstract graph state.
     * @param number the number of the state; required to be non-negative
     */
    public AbstractGraphState(CacheReference<StateCache> reference, int number) {
        super(reference);
        assert number >= 0;
        this.nr = number;
    }

    @Override
    public GTS getGTS() {
        return getRecord().getGTS();
    }

    @Override
    public int getTransitionCount() {
        return getCachedTransitionStubs().size();
    }

    public Iterator<GraphTransition> getTransitionIter() {
        // the iterator is created as a transformation of the iterator on the
        // stored OutGraphTransitions.
        return new TransformIterator<GraphTransitionStub,GraphTransition>(
            getTransitionStubIter()) {
            @Override
            public GraphTransition toOuter(GraphTransitionStub obj) {
                return obj.toTransition(AbstractGraphState.this);
            }
        };
    }

    public Set<GraphTransition> getTransitionSet() {
        return new TransformSet<GraphTransitionStub,GraphTransition>(
            getCachedTransitionStubs()) {
            @Override
            protected GraphTransition toOuter(GraphTransitionStub stub) {
                return stub.toTransition(AbstractGraphState.this);
            }

            @Override
            protected GraphTransitionStub toInner(Object key) {
                if (key instanceof GraphTransition) {
                    RuleEvent keyEvent = ((GraphTransition) key).getEvent();
                    HostNode[] keyAddedNodes =
                        ((GraphTransition) key).getAddedNodes();
                    GraphState keyTarget = ((GraphTransition) key).target();
                    return createTransitionStub(keyEvent, keyAddedNodes,
                        keyTarget);
                } else {
                    return null;
                }
            }
        };
    }

    public boolean containsTransition(GraphTransition transition) {
        return transition.source().equals(this)
            && getCachedTransitionStubs().contains(
                createTransitionStub(transition.getEvent(),
                    transition.getAddedNodes(), transition.target()));
    }

    @Override
    public Map<RuleEvent,GraphTransition> getTransitionMap() {
        return getCache().getTransitionMap();
    }

    /**
     * Add an outgoing transition to this state, if it is not yet there. Returns
     * the {@link GraphTransition} that was added, or <code>null</code> if no
     * new transition was added.
     */
    public boolean addTransition(GraphTransition transition) {
        return getCache().addTransitionStub(transition.toStub());
    }

    public Collection<? extends GraphState> getNextStateSet() {
        return new TransformSet<GraphTransitionStub,GraphState>(
            getCachedTransitionStubs()) {
            @Override
            public GraphState toOuter(GraphTransitionStub stub) {
                return stub.getTarget(AbstractGraphState.this);
            }
        };
    }

    public Iterator<? extends GraphState> getNextStateIter() {
        return new TransformIterator<GraphTransitionStub,GraphState>(
            getTransitionStubIter()) {
            @Override
            public GraphState toOuter(GraphTransitionStub obj) {
                return obj.getTarget(AbstractGraphState.this);
            }
        };
    }

    public GraphTransitionStub getOutStub(RuleEvent event) {
        assert event != null;
        GraphTransitionStub result = null;
        if (isClosed()) {
            GraphTransitionStub[] outTransitions = this.transitionStubs;
            for (int i = 0; result == null && i < outTransitions.length; i++) {
                GraphTransitionStub trans = outTransitions[i];
                if (trans.getEvent(this) == event) {
                    result = trans;
                }
            }
        } else {
            Iterator<GraphTransitionStub> outTransIter =
                getTransitionStubIter();
            while (result == null && outTransIter.hasNext()) {
                GraphTransitionStub trans = outTransIter.next();
                if (trans.getEvent(this) == event) {
                    result = trans;
                }
            }
        }
        return result;
    }

    /**
     * Callback factory method for creating an outgoing transition (from this
     * state) for the given derivation and target state. This implementation
     * invokes {@link #createInTransitionStub(GraphState, RuleEvent, HostNode[])} if
     * the target is a {@link AbstractGraphState}, otherwise it creates a
     * {@link IdentityTransitionStub}.
     */
    protected GraphTransitionStub createTransitionStub(RuleEvent event,
            HostNode[] addedNodes, GraphState target) {
        if (target instanceof AbstractGraphState) {
            return ((AbstractGraphState) target).createInTransitionStub(this,
                event, addedNodes);
        } else {
            return new IdentityTransitionStub(event, addedNodes, target);
        }
    }

    /**
     * Callback factory method for creating a transition stub to this state,
     * from a given graph and with a given rule event.
     */
    protected GraphTransitionStub createInTransitionStub(GraphState source,
            RuleEvent event, HostNode[] addedNodes) {
        return new IdentityTransitionStub(event, addedNodes, this);
    }

    /**
     * Returns an iterator over the outgoing transitions as stored, i.e.,
     * without encodings taken into account.
     */
    final protected Iterator<GraphTransitionStub> getTransitionStubIter() {
        if (isClosed()) {
            return getStoredTransitionStubs().iterator();
        } else {
            return getCachedTransitionStubs().iterator();
        }
    }

    /**
     * Returns a list view upon the current outgoing transitions.
     */
    private Set<GraphTransitionStub> getCachedTransitionStubs() {
        return getCache().getStubSet();
    }

    /**
     * Returns the collection of currently stored outgoing transition stubs.
     * Note that this is only guaranteed to be synchronised with the cached stub
     * set if the state is closed.
     */
    public final Collection<GraphTransitionStub> getStoredTransitionStubs() {
        return Arrays.asList(this.transitionStubs);
    }

    /**
     * Stores a set of outgoing transition stubs in a memory efficient way.
     */
    private void setStoredTransitionStubs(
            Collection<GraphTransitionStub> outTransitionSet) {
        if (outTransitionSet.isEmpty()) {
            this.transitionStubs = EMPTY_TRANSITION_STUBS;
        } else {
            this.transitionStubs =
                new GraphTransitionStub[outTransitionSet.size()];
            outTransitionSet.toArray(this.transitionStubs);
        }
    }

    public boolean isClosed() {
        return isCacheCollectable();
    }

    public boolean setClosed(boolean complete) {
        if (!isClosed()) {
            setStoredTransitionStubs(getCachedTransitionStubs());
            setCacheCollectable();
            updateClosed();
            // reset the schedule to the beginning if the state was not 
            // completely explored
            if (!complete) {
                setSchedule(getCtrlState().getSchedule());
            }
            return true;
        } else {
            return false;
        }
    }

    /** Callback method to notify that the state was closed. */
    abstract protected void updateClosed();

    /**
     * Retrieves a frozen representation of the graph, in the form of all nodes
     * and edges collected in one array. May return <code>null</code> if there
     * is no frozen representation.
     * @return All nodes and edges of the graph, or <code>null</code>
     */
    HostElement[] getFrozenGraph() {
        return this.frozenGraph;
    }

    /** Stores a frozen representation of the graph. */
    void setFrozenGraph(HostElement[] frozenGraph) {
        this.frozenGraph = frozenGraph;
        frozenGraphCount++;
    }

    /**
     * This implementation compares state numbers. The current state is either
     * compared with the other, if that is a {@link AbstractGraphState}, or
     * with its source state if it is a {@link DefaultGraphTransition}.
     * Otherwise, the method throws an {@link UnsupportedOperationException}.
     */
    public int compareTo(Element obj) {
        if (obj instanceof AbstractGraphState) {
            return getNumber() - ((AbstractGraphState) obj).getNumber();
        } else if (obj instanceof DefaultGraphTransition) {
            return getNumber()
                - ((AbstractGraphState) ((DefaultGraphTransition) obj).source()).getNumber();
        } else {
            throw new UnsupportedOperationException(String.format(
                "Classes %s and %s cannot be compared", getClass(),
                obj.getClass()));
        }
    }

    /**
     * Returns a name for this state, rather than a full description. To get the
     * full description, use <tt>DefaultGraph.toString(Graph)</tt>.
     * 
     * @see groove.graph.AbstractGraph#toString(Graph)
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("s");
        if (hasNumber()) {
            result.append(getNumber());
        } else {
            result.append("??");
        }
        if (getBoundNodes().length > 0) {
            result.append(Arrays.toString(getBoundNodes()));
        }
        return result.toString();
    }

    /**
     * Callback factory method for a new cache based on this state.
     */
    @Override
    protected StateCache createCache() {
        return new StateCache(this);
    }

    /**
     * Stores the transitions from the cache, if the state is not already
     * closed.
     */
    @Override
    public void clearCache() {
        if (!isClosed()) {
            setStoredTransitionStubs(getCachedTransitionStubs());
        }
        super.clearCache();
    }

    /** Indicates whether the state has already been assigned a number. */
    protected boolean hasNumber() {
        return this.nr >= 0;
    }

    /**
     * Returns the number of this state. The number is meant to be unique for
     * each state in a given transition system.
     * @throws IllegalStateException if {@link #hasNumber()} returns
     *         <code>false</code> at the time of calling
     */
    public int getNumber() {
        return this.nr;
    }

    /** Returns the system record associated with this state. */
    SystemRecord getRecord() {
        return ((StateReference) getCacheReference()).getRecord();
    }

    /**
     * Returns the map of parameters to nodes for this state
     * @return a Map<String,Node> of parameters
     */
    public HostNode[] getBoundNodes() {
        return EMPTY_NODE_LIST;
    }

    /** 
     * Sets the control schedule.
     * This should occur at initialisation.
     */
    protected final void setCtrlState(CtrlState ctrlState) {
        this.schedule = ctrlState.getSchedule();
    }

    public CtrlState getCtrlState() {
        return this.schedule.getState();
    }

    @Override
    public void setSchedule(CtrlSchedule schedule) {
        assert schedule.getState() == getCtrlState();
        this.schedule = schedule;
    }

    public final CtrlSchedule getSchedule() {
        return this.schedule;
    }

    /** The underlying control state, if any. */
    private CtrlSchedule schedule;

    /** Global constant empty stub array. */
    private GraphTransitionStub[] transitionStubs = EMPTY_TRANSITION_STUBS;

    /**
     * Slot to store a frozen graph representation. When filled, this provides a
     * faster way to reconstruct the graph of this state.
     */
    private HostElement[] frozenGraph;
    /**
     * The number of this Node.
     * 
     * @invariant nr < nrNodes
     */
    private final int nr;

    /** Returns the total number of fixed delta graphs. */
    static public int getFrozenGraphCount() {
        return frozenGraphCount;
    }

    /** The total number of delta graphs frozen. */
    static private int frozenGraphCount;

    /** Constant empty array of out transition, shared for memory efficiency. */
    private static final GraphTransitionStub[] EMPTY_TRANSITION_STUBS =
        new GraphTransitionStub[0];
    static final HostNode[] EMPTY_NODE_LIST = new HostNode[0];
}