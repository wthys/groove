<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n26"/>
        <node id="n27"/>
        <node id="n28"/>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
    </graph>
</gxl>
