<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n10467"/>
        <node id="n10469"/>
        <node id="n10468"/>
        <node id="n10465"/>
        <node id="n10463"/>
        <node id="n10464"/>
        <node id="n10466"/>
        <edge from="n10463" to="n10463">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n10469" to="n10469">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n10467" to="n10468">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n10463" to="n10463">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n10464" to="n10464">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n10465" to="n10468">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge from="n10466" to="n10466">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge from="n10468" to="n10469">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n10463" to="n10468">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n10467" to="n10467">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n10465" to="n10465">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge from="n10467" to="n10464">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n10467" to="n10465">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n10467" to="n10467">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n10463" to="n10466">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n10463" to="n10465">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
    </graph>
</gxl>
