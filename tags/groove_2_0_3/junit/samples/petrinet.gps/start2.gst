<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n5"/>
        <node id="n2"/>
        <node id="n11"/>
        <node id="n23"/>
        <node id="n15"/>
        <node id="n26"/>
        <node id="n29"/>
        <node id="n7"/>
        <node id="n32"/>
        <node id="n12"/>
        <node id="n27"/>
        <node id="n9"/>
        <node id="n34"/>
        <node id="n24"/>
        <node id="n14"/>
        <node id="n21"/>
        <node id="n18"/>
        <node id="n36"/>
        <node id="n1"/>
        <node id="n33"/>
        <node id="n28"/>
        <node id="n13"/>
        <node id="n30"/>
        <node id="n6"/>
        <node id="n20"/>
        <node id="n10"/>
        <node id="n22"/>
        <node id="n8"/>
        <node id="n31"/>
        <node id="n16"/>
        <node id="n3"/>
        <node id="n19"/>
        <node id="n4"/>
        <node id="n0"/>
        <node id="n25"/>
        <node id="n35"/>
        <node id="n17"/>
        <edge from="n35" to="n34">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n31" to="n10">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n13" to="n13">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>token</string>
            </attr>
        </edge>
        <edge from="n12" to="n2">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n28" to="n16">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n3" to="n0">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n26" to="n15">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n21" to="n13">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n13" to="n26">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n23" to="n16">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n21" to="n1">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n8" to="n25">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n9" to="n21">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n2" to="n3">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n1" to="n26">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n30" to="n29">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n4" to="n32">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n16" to="n1">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n4" to="n22">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n34" to="n24">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n11" to="n12">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n19" to="n14">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n10" to="n14">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n29" to="n17">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n15" to="n11">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n2" to="n18">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n2" to="n7">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n5" to="n28">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n29" to="n30">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n26" to="n35">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n24" to="n2">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n32" to="n16">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>token</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n36" to="n29">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n6" to="n19">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n20" to="n6">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n14" to="n23">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n30" to="n27">
            <attr name="label">
                <string>mark</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n22" to="n5">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n25" to="n4">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n29" to="n20">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n7" to="n8">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n17" to="n31">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n9" to="n33">
            <attr name="label">
                <string>mark</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n0" to="n13">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n8" to="n36">
            <attr name="label">
                <string>out</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
        <edge from="n18" to="n8">
            <attr name="label">
                <string>in</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>place</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>transition</string>
            </attr>
        </edge>
    </graph>
</gxl>
