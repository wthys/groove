<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n247"/>
        <node id="n248"/>
        <node id="n241"/>
        <node id="n245"/>
        <node id="n246"/>
        <node id="n244"/>
        <node id="n243"/>
        <node id="n239"/>
        <node id="n240"/>
        <node id="n242"/>
        <node id="n249"/>
        <edge from="n245" to="n247">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n241" to="n241">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n241" to="n243">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n240" to="n240">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge from="n249" to="n249">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n243" to="n248">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n248" to="n242">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n245" to="n243">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n241" to="n247">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n244" to="n246">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n248" to="n244">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n245" to="n245">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n243" to="n239">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n241" to="n241">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n247" to="n247">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge from="n246" to="n246">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge from="n245" to="n240">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n239" to="n239">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n247" to="n243">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge from="n245" to="n245">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n241" to="n249">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n242" to="n242">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
    </graph>
</gxl>
