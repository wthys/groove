// GROOVE: GRaphs for Object Oriented VErification
// Copyright 2003--2007 University of Twente
 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// http://www.apache.org/licenses/LICENSE-2.0 
 
// Unless required by applicable law or agreed to in writing, 
// software distributed under the License is distributed on an 
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
// either express or implied. See the License for the specific 
// language governing permissions and limitations under the License.
/*
 * $Id: DeltaGraphTest.java,v 1.3 2007-09-25 22:57:51 rensink Exp $
 */
package groove.test.graph;

import groove.graph.AbstractGraph;
import groove.graph.DeltaGraph;
import groove.graph.Graph;

import java.io.File;

/**
 * Test class to test <tt>DeltaGraph</tt>
 * @author Arend Rensink
 * @version $Revision: 1.3 $
 */
public class DeltaGraphTest extends GraphTest {
    public DeltaGraphTest(String name) {
        super(name);
    }
    
    @Override
    protected Graph loadGraph(File file) throws Exception {
       AbstractGraph basis = (AbstractGraph) super.loadGraph(file);
       basis.setFixed();
        return new DeltaGraph(basis);
    }
}
