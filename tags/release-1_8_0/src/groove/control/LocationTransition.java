/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2007 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id: LocationTransition.java,v 1.4 2007-08-26 07:23:33 rensink Exp $
 */
package groove.control;


/*

public class LocationTransition implements Transition {

	private NameLabel label;
	
	
	private GraphState source;
	private GraphState target;
	
	public LocationTransition(GraphState source, GraphState target)
	{
		this.source = source;
		this.target = target; 
		this.label = new NameLabel("_");
	}
	
	public GraphState source() {
		// TODO Auto-generated method stub
		return source;
	}

	public GraphState target() {
		// TODO Auto-generated method stub
		return target;
	}

	public int compareTo(Element obj) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Node end(int i) {
		// TODO Auto-generated method stub
		return target;
	}

	public int endCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	public int endIndex(Node node) {
		// TODO Auto-generated method stub
		return 1;
	}

	public Node[] ends() {
		// TODO Auto-generated method stub
		return new Node[]{target};
	}

	public boolean hasEnd(Node node) {
		// TODO Auto-generated method stub
		return (node == target);
	}

	public Edge imageFor(NodeEdgeMap elementMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public Label label() {
		// TODO Auto-generated method stub
		return label;
	}

	public Node opposite() {
		// TODO Auto-generated method stub
		return target;
	}

}
*/
