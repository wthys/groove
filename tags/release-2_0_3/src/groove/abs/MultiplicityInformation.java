package groove.abs;

/** Represents a single multiplicity, or a set of 
 * multiplicities of the form {i, i+1, ..., omega}.
 */
public interface MultiplicityInformation {
	//
}
