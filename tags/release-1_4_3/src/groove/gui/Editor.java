// GROOVE: GRaphs for Object Oriented VErification
// Copyright 2003--2007 University of Twente
 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// http://www.apache.org/licenses/LICENSE-2.0 
 
// Unless required by applicable law or agreed to in writing, 
// software distributed under the License is distributed on an 
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
// either express or implied. See the License for the specific 
// language governing permissions and limitations under the License.
/*
 * $Id: Editor.java,v 1.6 2007-04-04 07:04:27 rensink Exp $
 */
package groove.gui;

import static groove.gui.Options.IS_ATTRIBUTED_OPTION;
import groove.graph.Graph;
import groove.gui.jgraph.EditorJGraph;
import groove.gui.jgraph.EditorJModel;
import groove.gui.jgraph.GraphJModel;
import groove.gui.jgraph.JGraph;
import groove.gui.jgraph.AspectJModel;
import groove.io.ExtensionFilter;
import groove.io.GrooveFileChooser;
import groove.io.LayedOutXml;
import groove.io.Xml;
import groove.util.FormatException;
import groove.trans.DefaultRuleFactory;
import groove.trans.NameLabel;
import groove.trans.RuleFactory;
import groove.trans.RuleProperties;
import groove.trans.view.AspectualRuleView;
import groove.util.Converter;
import groove.util.Groove;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.WindowConstants;
import javax.swing.event.UndoableEditEvent;

import org.jgraph.event.GraphModelEvent;
import org.jgraph.event.GraphModelListener;
import org.jgraph.event.GraphSelectionEvent;
import org.jgraph.event.GraphSelectionListener;
import org.jgraph.graph.GraphUndoManager;

/**
 * Simplified but usable graph editor.
 * @author Gaudenz Alder, modified by Arend Rensink and Carel van Leeuwen
 * @version $Revision: 1.6 $ $Date: 2007-04-04 07:04:27 $
 */
public class Editor extends JFrame implements GraphModelListener, IEditorModes {
    /** The name of the editor application. */
    public static final String EDITOR_NAME = "Groove Editor";
    /** The name displayed in the frame title for a new graph. */
    public static final String NEW_GRAPH_NAME = "New Graph";
    /** The indication displayed in the frame title for a modified graph. */
    public static final String MODIFIED_INDICATOR = "> ";
    /** Size of the preview dialog window. */
    private static final Dimension PREVIEW_SIZE = new Dimension(500, 500);

    /**
     * @param args empty or a singleton containing a filename of the graph to be edited
     */
    public static void main(String[] args) {
        try {
            // Add an Editor Panel
            final Editor editor = new Editor();
            if (args.length == 0) {
                editor.setModel(null, null);
            } else {
                editor.doOpenGraph(new File(args[0]));
            }
            editor.setVisible(true);
        } catch (IOException exc) {
            System.out.println("Error: " + exc.getMessage());
        }
    }

    /**
     * @param owner
     * @param modal
     * @param editor
     * @return editor dialog
     */
    static public JDialog createEditorDialog(JFrame owner, boolean modal, Editor editor) {
        JDialog result = new JDialog(owner, modal);
        result.setJMenuBar(editor.getJMenuBar());
        result.setContentPane(editor.getContentPane());
        result.setTitle(editor.getTitle());
        result.pack();
        return result;
    }
    
    /**
     * Action to set the editing mode (selection, node or edge).
     */
    protected class SetEditingModeAction extends ToolbarAction {
    	/** Constructs an action with a given name, key and icon. */
        SetEditingModeAction(String text, KeyStroke acceleratorKey, ImageIcon smallIcon) {
            super(text, acceleratorKey, smallIcon);
            putValue(SHORT_DESCRIPTION, null);
        }
    
        /** (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent evt) {
            super.actionPerformed(evt);
            updateModeButtons(this);
        }
    }

    /**
     * Action to preview the current jgraph as a transformation rule.
     */
    protected class RulePreviewAction extends ToolbarAction {
    	/** Constructs an instance of the action. */
        protected RulePreviewAction() {
            super(Options.VIEW_ACTION_NAME, null, Groove.RULE_SMALL_ICON);
        }
    
        /** (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent evt) {
            super.actionPerformed(evt);
            handlePreview();
        }
    }

    /**
     * Action to export the current state of the editor to an image file.
     */
    protected class ExportGraphAction extends AbstractAction {
    	/** Constructs an instance of the action. */
        protected ExportGraphAction() {
            super(Options.EXPORT_ACTION_NAME);
            putValue(ACCELERATOR_KEY, Options.EXPORT_KEY);
        }
    
        /** (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent evt) {
            handleExportGraph();
        }
    }

    /**
     * Action to save the current state of the editor into a file.
     */
    protected class SaveGraphAction extends ToolbarAction {
    	/** Constructs an instance of the action. */
        protected SaveGraphAction() {
            super(Options.SAVE_ACTION_NAME, Options.SAVE_KEY, new ImageIcon(Groove.getResource("save.gif")));
        }
    
        @Override
        public void actionPerformed(ActionEvent evt) {
            super.actionPerformed(evt);
            handleSaveGraph();
        }
    }

    /**
     * Action to open a graph file into the editor.
     */
    protected class OpenGraphAction extends ToolbarAction {
    	/** Constructs an instance of the action. */
        protected OpenGraphAction() {
            super(Options.OPEN_ACTION_NAME, Options.OPEN_KEY, new ImageIcon(Groove.getResource("open.gif")));
        }
    
        @Override
        public void actionPerformed(ActionEvent evt) {
            super.actionPerformed(evt);
            handleOpenGraph();
        }
    }

    /**
     * Action to start with a blank graph.
     */
    protected class NewGraphAction extends ToolbarAction {
    	/** Constructs an instance of the action. */
        NewGraphAction() {
            super(Options.NEW_ACTION_NAME, Options.NEW_KEY, new ImageIcon(Groove.getResource("new.gif")));
        }
    
        @Override
        public void actionPerformed(ActionEvent evt) {
            super.actionPerformed(evt);
            if (showAbandonDialog()) {
                currentFile = null;
                setModel(null, null);
                getGraphSaveChooser().setSelectedFile(null);
            }
        }
    }

    /** This will change the source of the actionevent to graph. */
    protected class TransferAction extends ToolbarAction {
        /**
         * Constructs an action that redirects to another action, while 
         * seting the source of the event to the editor's j-graph.
         */
        public TransferAction(Action action, KeyStroke acceleratorKey, String name) {
            super(name, acceleratorKey, (ImageIcon) action.getValue(SMALL_ICON));
            putValue(SHORT_DESCRIPTION, name);
            setEnabled(false);
            this.action = action;
        }
    
        /** Redirects the Actionevent. */
        @Override
        public void actionPerformed(ActionEvent evt) {
            super.actionPerformed(evt);
            evt = new ActionEvent(jgraph, evt.getID(), evt.getActionCommand(), evt.getModifiers());
            action.actionPerformed(evt);
            if (this == cutAction || this == copyAction) {
                pasteAction.setEnabled(true);
            }
        }
        
        /** The action that this transfer action wraps. */
        protected Action action;        
    }

    /**
     * General class for actions with toolbar buttons. Takes care of image, name and key
     * accelleration; moreover, the <tt>actionPerformed(ActionEvent)</tt> starts by invoking
     * <tt>stopEditing()</tt>.
     * @author Arend Rensink
     * @version $Revision: 1.6 $
     */
    protected abstract class ToolbarAction extends AbstractAction {
    	/** Constructs an action with a given name, key and icon. */
        ToolbarAction(String name, KeyStroke acceleratorKey, Icon icon) {
            super(name, icon);
            putValue(Action.SHORT_DESCRIPTION, name);
            putValue(ACCELERATOR_KEY, acceleratorKey);
            jGraphPanel.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
                    .put(acceleratorKey, name);
            jgraph.getInputMap().put(acceleratorKey, name);
            jGraphPanel.getActionMap().put(name, this);
        }
    
        public void actionPerformed(ActionEvent evt) {
            jgraph.stopEditing();
        }
    }

    /**
     * Action for quitting the editor.
     * Calls {@link Editor#handleQuit()} to execute the action.
     */
    protected class QuitAction extends AbstractAction {
    	/** Constructs an instance of the action. */
        public QuitAction() {
            super(Options.QUIT_ACTION_NAME);
            putValue(ACCELERATOR_KEY, Options.QUIT_KEY);
        }
    
        /**
         * Calls {@link Editor#handleQuit()}.
         */
        public void actionPerformed(ActionEvent e) {
            handleQuit();
        }
    
    }
    
    /**
     * An action to close the editor, used if the editor is invoked in the
     * context of some other frame.
     * Closing is done using {@link Editor#handleClose()}.
     */
    protected class CloseEditorAction extends AbstractAction {
    	/** Constructs an instance of the action. */
        public CloseEditorAction() {
            super(Options.STOP_EDIT_ACTION_NAME);
        }
        
        /** Callse {@link Editor#handleClose()}. */
        public void actionPerformed(ActionEvent e) {
            handleClose();
        }
    }

    /** 
     * Constructs an editor frame with an initially empty graph.
     * It is not configured as an auxiliary component.
     * @see #isAuxiliary()  
     */
    public Editor() {
        this(false);
    }

    /** 
     * Constructs an editor frame with an initially empty graph,
     * possibly for use as an auxiliary component in another frame.
     * @param auxiliary <tt>true</tt> if the editor is an auxiliary component
     * (in which case it should not exit on closing)
     * @ensure <tt>isAuxiliary() == auxiliary</tt>
     */
    public Editor(boolean auxiliary) {
        super(EDITOR_NAME);
        this.auxiliary = auxiliary;
        // set file chooser for load, save & exit
        currentDir = new File(Groove.WORKING_DIR);
        // Construct the main components
        jgraph = new EditorJGraph(this);
        jGraphPanel = new JGraphPanel<EditorJGraph>(jgraph);

        initListeners();
        initActions();
        initGUI();
        pack();
    }

    /**
     * Indicates if this editor is used as an auxiliary component in
     * some other frame.
     */
    public boolean isAuxiliary() {
        return auxiliary;
    }

    /**
     * Indicates whether the editor is in node editing mode.
     * @return <tt>true</tt> if the editor is in node editing mode.
     */
    public boolean isNodeMode() {
        return getNodeModeButton().isSelected();
    }

    /**
     * Indicates whether the editor is in edge editing mode.
     * @return <tt>true</tt> if the editor is in edge editing mode.
     */
    public boolean isEdgeMode() {
        return getEdgeModeButton().isSelected();
    }

    /**
     * Reads the graph to be edited from a file.
     * If the file does not exist, a new, empty model with the given name is created.
     * @param fromFile the file to read from
     * @throws IOException if <tt>fromFile</tt> did not contain a correctly formatted graph
     */
    public void doOpenGraph(final File fromFile) throws IOException {
        currentFile = fromFile;
        currentDir = currentFile.getAbsoluteFile().getParentFile();
        // first create a graph from the gxl file
        Graph graph = null;
        try {
            graph = layoutGxl.unmarshalGraph(fromFile);
        } catch (FormatException e) {
            throw new IOException("Can't load graph from " + fromFile.getName() + ": format error");
        } catch (FileNotFoundException e) {
            // we create a new model
        }
        // load the model in the event dispatch thread, to avoid concurrency issues
        final GraphJModel model = graph == null ? null : new GraphJModel(graph, getOptions());
        SwingUtilities.invokeLater(new Runnable() {
			public void run() {
		        setModel(fromFile.getName(), model);
			}        	
        });
    }

    /**
     * Saves the currently edited model as an ordinary graph to a file.
     * @param toFile the file to save to
     * @throws IOException if <tt>fromFile</tt> did not contain a correctly formatted graph
     */
    public void doSaveGraph(File toFile) throws FormatException, IOException {
        currentFile = toFile;
        currentDir = toFile.getParentFile();
        Graph saveGraph = getModel().toPlainGraph();
        layoutGxl.marshalGraph(saveGraph, toFile);
        setModelName(currentFile.getName());
        setJGraphModified(false);
    }

    /**
     * Exports the currently edited model, including hidden and emphasis, to an image file.
     * @param filter the filter that determines the format to export to
     * @param toFile the file to save to
     * @throws IOException if <tt>fromFile</tt> did not contain a correctly formatted graph
     */
    public void doExportGraph(ExtensionFilter filter, File toFile) throws IOException {
        if (filter == fsmFilter) {
            PrintWriter writer = new PrintWriter(new FileWriter(toFile));
            Converter.graphToFsm(getModel().toPlainGraph(), writer);
            writer.close();
        } else {
            String formatName = filter.getExtension().substring(1);
            Iterator<ImageWriter> writerIter = ImageIO.getImageWritersBySuffix(formatName);
            if (writerIter.hasNext()) {
                ImageIO.write(jgraph.toImage(), formatName, toFile);
            } else {
                showErrorDialog("No image writer found for " + filter.getDescription(), null);
            }
        }
    }

    /**
     * Changes the graph being edited to a given j-model, with a given name. If the model is
     * <tt>null</tt>, a fresh {@link EditorJModel}is created; otherwise, the given j-model is
     * copied into a new {@link EditorJModel}.
     * @param name the name of the model; if <tt>null</tt>, the graph has no name
     * @param model the j-model to be set; if <tt>null</tt>, a fresh {@link EditorJModel}is
     *        created
     * @see EditorJModel#EditorJModel()
     * @see EditorJModel#EditorJModel(String, GraphJModel)
     */
    public void setModel(String name, GraphJModel model) {
        // unregister listeners with the model
        getModel().removeUndoableEditListener(undoManager);
        getModel().removeGraphModelListener(this);
        if (model == null) {
            jgraph.setModel(new EditorJModel(name));
        } else {
            jgraph.setModel(new EditorJModel(name, model));
        }
        setJGraphModified(false);
        undoManager.discardAllEdits();
        getModel().addUndoableEditListener(undoManager);
        getModel().addGraphModelListener(this);
        updateHistoryButtons();
        updateStatus();
    }
    
    /**
     * @return the j-model currently being edited, or 
     * <tt>null</tt> if no editor model is set.
     */
    public EditorJModel getModel() {
        return jgraph.getModel();
    }

    /** Records the current number of visible graph elements on the status bar. 
     *  (non-Javadoc)
     * @see org.jgraph.event.GraphModelListener#graphChanged(org.jgraph.event.GraphModelEvent)
     */
    public void graphChanged(GraphModelEvent e) {
        updateStatus();
    }

    /**
     * Handler method to execute a {@link OpenGraphAction}.
     * Invokes a file chooser dialog, and calls {@link #doOpenGraph(File)} 
     * if a file is selected. 
     */
    protected void handleOpenGraph() {
        getGraphOpenChooser().setCurrentDirectory(currentDir);
        getGraphOpenChooser().setSelectedFile(null);
        int result = getGraphOpenChooser().showOpenDialog(jGraphPanel);
        if (result == JFileChooser.APPROVE_OPTION && showAbandonDialog()) {
            try {
                doOpenGraph(getGraphOpenChooser().getSelectedFile());
            } catch (IOException exc) {
                // exc.printStackTrace(System.err);
                showErrorDialog("Error while loading graph from " + currentFile, exc);
            }
        }
    }

    /**
     * Handler method to execute a {@link SaveGraphAction}.
     * Invokes a file chooser dialog, and calls {@link #doSaveGraph(File)} 
     * if a file is selected. 
     */
    protected File handleSaveGraph() {
        // set filter to the one that accepts the current file (if any)
        javax.swing.filechooser.FileFilter[] fileFilters = getGraphSaveChooser().getChoosableFileFilters();
        boolean filterFound = false;
        for (int i = 0; !filterFound && i < fileFilters.length; i++) {
            if (currentFile == null || fileFilters[i].accept(currentFile)) {
                filterFound = true;
                getGraphSaveChooser().setFileFilter(fileFilters[i]);
            }
        }
        getGraphSaveChooser().setCurrentDirectory(currentDir);
        getGraphSaveChooser().setSelectedFile(currentFile);
        // get the file to write to
        File toFile = ExtensionFilter.showSaveDialog(getGraphSaveChooser(), jGraphPanel);
        if (toFile != null) {
            try {
                if (getGraphSaveChooser().getFileFilter() == ruleFilter
                        && getConfirmPreviewCheckBox().isSelected()) {
                    if (handlePreview()) {
                        doSaveGraph(toFile);
                    }
                } else {
                    doSaveGraph(toFile);
                }
            } catch (Exception exc) {
                showErrorDialog("Error while saving graph to " + currentFile, exc);
                toFile = null;
            }
        }
        return toFile;
    }

    /**
     * Handler method to execute a {@link ExportGraphAction}.
     * Invokes a file chooser dialog, and calls {@link #doSaveGraph(File)} 
     * if a file is selected. 
     */
    protected void handleExportGraph() {
        if (getModelName() != null) {
            getExportChooser().setSelectedFile(new File(graphFilter.stripExtension(getModelName())));
        }
        File toFile = ExtensionFilter.showSaveDialog(getExportChooser(), jGraphPanel);
        if (toFile != null) {
            ExtensionFilter filter = (ExtensionFilter) getExportChooser().getFileFilter();
            try {
                doExportGraph(filter, toFile);
            } catch (IOException exc) {
                showErrorDialog("Error while saving to " + toFile, exc);
            }
        }
    }

    /**
     * Shows a preview dialog; if confirmed, changes the model to a new layout.
     * @return <tt>true</tt> if the dialog was confirmed
     */
	protected boolean handlePreview() {
	    try {
	    	NameLabel ruleName = new NameLabel("temp");
            AspectualRuleView ruleGraph = (AspectualRuleView) getRuleFactory().createRuleView(getModel().toPlainGraph(), ruleName, 0, getRuleProperties());
            AspectJModel ruleModel = new AspectJModel(ruleGraph, getOptions());
            JGraph previewGraph = new JGraph(ruleModel);
            JOptionPane previewPane = new JOptionPane(new JScrollPane(previewGraph),
                    JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
            JDialog previewDialog = previewPane.createDialog(jGraphPanel, "Production rule view");
            previewDialog.setSize(PREVIEW_SIZE);
            previewGraph.setToolTipEnabled(true);
            previewDialog.setVisible(true);
            Integer answer = (Integer) previewPane.getValue();
            if (answer != null && answer.intValue() == JOptionPane.OK_OPTION) {
                setSelectInsertedCells(false);
                getModel().replace(new GraphJModel(ruleModel.toPlainGraph(), getOptions()));
                setSelectInsertedCells(true);
                return true;
            }
        } catch (FormatException exc) {
            showErrorDialog("Error in graph format", exc);
        }
        return false;
	}

    /**
     * Closes the editor by making the root component invisible.
     */
    protected void handleClose() {
        getRootComponent().setVisible(false);
    }

    /**
     * If the editor has unsaved changes, asks if these should be abandoned;
     * then calls {@link #dispose()}.
     */
    protected void handleQuit() {
        if (showAbandonDialog()) {
            dispose();
            // calling exit is too rigorous
            // System.exit(0);
        }
    }
    
    /** Initialises the graph selection listener and attributed graph listener. */
    protected void initListeners() {
        jgraph.setToolTipEnabled(true);
        // Update ToolBar based on Selection Changes
        jgraph.getSelectionModel().addGraphSelectionListener(new GraphSelectionListener() {
            public void valueChanged(GraphSelectionEvent e) {
                // Update Button States based on Current Selection
                boolean enabled = !jgraph.isSelectionEmpty();
                deleteAction.setEnabled(enabled);
                copyAction.setEnabled(enabled);
                cutAction.setEnabled(enabled);
            }
        });

		final JCheckBoxMenuItem attributedGraphsItem = getOptions().getItem(Options.IS_ATTRIBUTED_OPTION);
		// listen to the option controlling the parsing of attributed graphs
		attributedGraphsItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setRuleFactory(DefaultRuleFactory.getInstance());
			}
		});
    }

    /**
     * Initializes the actions, including keyboard shortcuts.
     */
    protected void initActions() {
        // actions if the component is not auxiliary
        if (auxiliary) {
            closeAction = new CloseEditorAction();
        } else {
            newAction = new NewGraphAction();
            openAction = new OpenGraphAction();
            saveAction = new SaveGraphAction();
            exportAction = new ExportGraphAction();
            quitAction = new QuitAction();
        }
        // Set selection mode
        ImageIcon selectIcon = new ImageIcon(Groove.getResource("select.gif"));
        selectModeAction = new SetEditingModeAction(Options.SELECT_MODE_NAME,
                Options.SELECT_MODE_KEY, selectIcon);
        ImageIcon nodeIcon = new ImageIcon(Groove.getResource("rectangle.gif"));
        nodeModeAction = new SetEditingModeAction(Options.NODE_MODE_NAME, Options.NODE_MODE_KEY,
                nodeIcon);
        ImageIcon edgeIcon = new ImageIcon(Groove.getResource("edge.gif"));
        edgeModeAction = new SetEditingModeAction(Options.EDGE_MODE_NAME, Options.EDGE_MODE_KEY,
                edgeIcon);

        // Undo
        ImageIcon undoIcon = new ImageIcon(Groove.getResource("undo.gif"));
        undoAction = new ToolbarAction(Options.UNDO_ACTION_NAME, Options.UNDO_KEY, undoIcon) {
            @Override
            public void actionPerformed(ActionEvent evt) {
                super.actionPerformed(evt);
                undoLastEdit();
            }
        };
        undoAction.setEnabled(false);

        // Redo
        ImageIcon redoIcon = new ImageIcon(Groove.getResource("redo.gif"));
        redoAction = new ToolbarAction(Options.REDO_ACTION_NAME, Options.REDO_KEY, redoIcon) {
            @Override
            public void actionPerformed(ActionEvent evt) {
                super.actionPerformed(evt);
                redoLastEdit();
            }
        };
        redoAction.setEnabled(false);
        // Create a GraphUndoManager which also Updates the ToolBar
        undoManager = new GraphUndoManager() {
            @Override
            public void undoableEditHappened(UndoableEditEvent e) {
                super.undoableEditHappened(e);
                updateHistoryButtons();
            }
        };

        Action action;

        // Copy
        action = TransferHandler.getCopyAction();
        action.putValue(Action.SMALL_ICON, new ImageIcon(Groove.getResource("copy.gif")));
        copyAction = new TransferAction(action, Options.COPY_KEY, Options.COPY_ACTION_NAME);

        // Paste
        action = TransferHandler.getPasteAction();
        action.putValue(Action.SMALL_ICON, new ImageIcon(Groove.getResource("paste.gif")));
        pasteAction = new TransferAction(action, Options.PASTE_KEY, Options.PASTE_ACTION_NAME);

        // Cut
        action = TransferHandler.getCutAction();
        action.putValue(Action.SMALL_ICON, new ImageIcon(Groove.getResource("cut.gif")));
        action.putValue(Action.ACCELERATOR_KEY, Options.CUT_KEY);
        cutAction = new TransferAction(action, Options.CUT_KEY, Options.CUT_ACTION_NAME);

        // Remove
        ImageIcon deleteIcon = new ImageIcon(Groove.getResource("delete.gif"));
        deleteAction = new ToolbarAction(Options.DELETE_ACTION_NAME, Options.DELETE_KEY, deleteIcon) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!jgraph.isSelectionEmpty()) {
                    Object[] cells = jgraph.getSelectionCells();
                    cells = jgraph.getDescendants(cells);
                    jgraph.getModel().remove(cells);
                }
            }
        };
        deleteAction.setEnabled(false);
    }

    /** Initialises the GUI. */
    protected void initGUI() {
        JPanel editorPanel = createEditor();
        setIconImage(Groove.GROOVE_ICON_16x16.getImage());
        // Set Close Operation to Exit
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                handleQuit();
            }
        });
        setJMenuBar(createMenuBar());
        setContentPane(editorPanel);
    }

    /**
     * @return the main editor panel
     */
    public JPanel createEditor() {
        JPanel result = new JPanel();
        // initialize the main editor panel
        // Use Border Layout
        result.setLayout(new BorderLayout());
        // Add the main pane as Center Component
        // initEditorPane(createSplitEditorPane());
        // Add a ToolBar
        editorToolBar = createToolBar();
        result.add(editorToolBar, BorderLayout.NORTH);
        result.add(jGraphPanel, BorderLayout.CENTER);
        result.add(statusBar, BorderLayout.SOUTH);
        return result;
    }

	/**
	 * Returns a file chooser for exporting graphs, after lazily creating it.
	 */
	protected JFileChooser getExportChooser() {
		if (exportChooser == null) {
			exportChooser = new GrooveFileChooser();
			exportChooser.setAcceptAllFileFilterUsed(false);
			exportChooser.addChoosableFileFilter(fsmFilter);
			exportChooser.addChoosableFileFilter(jpgFilter);
			exportChooser.addChoosableFileFilter(pngFilter);
			exportChooser.addChoosableFileFilter(epsFilter);
			exportChooser.setFileFilter(pngFilter);
			exportChooser.setCurrentDirectory(new File(Groove.WORKING_DIR));
		}
		return exportChooser;
	}

	/**
	 * Returns a file chooser for loading graphs, after lazily creating it.
	 */
	protected JFileChooser getGraphOpenChooser() {
		if (graphOpenChooser == null) {
			graphOpenChooser = new GrooveFileChooser();
			graphOpenChooser.addChoosableFileFilter(graphFilter);
		}
		return graphOpenChooser;
	}

	/**
	 * Returns a file chooser for saving graphs, after lazily creating it.
	 */
	protected JFileChooser getGraphSaveChooser() {
		if (graphSaveChooser == null) {
			graphSaveChooser = new GrooveFileChooser();
			graphSaveChooser.setAcceptAllFileFilterUsed(false);
			graphSaveChooser.addChoosableFileFilter(stateFilter);
			graphSaveChooser.addChoosableFileFilter(ruleFilter);
			graphSaveChooser.addChoosableFileFilter(gxlFilter);
			graphSaveChooser.setFileFilter(stateFilter);
			JPanel checkBoxPanel = new JPanel(new BorderLayout());
			checkBoxPanel.setBorder(new javax.swing.border.EmptyBorder(0, 5, 0,
					0));
			checkBoxPanel.add(getConfirmPreviewCheckBox(), BorderLayout.SOUTH);
			graphSaveChooser.setAccessory(checkBoxPanel);
		}
		return graphSaveChooser;
	}

	/**
	 * Returns a checkbox forcing a preview dialog before saving a rule.
	 */
	protected JCheckBox getConfirmPreviewCheckBox() {
		if (confirmPreviewCheckBox == null) {
			confirmPreviewCheckBox = new JCheckBox();
			confirmPreviewCheckBox.setText("Rule preview");
			confirmPreviewCheckBox.setSelected(true);
		}
		return confirmPreviewCheckBox;
	}
    
    /**
	 * Sets the modified status of the underlying jgraph. Also updates the frame
	 * title to reflect the new modified status.
	 * 
	 * @param modified
	 *            the new modified status
	 * @see #isJGraphModified()
	 */
    protected void setJGraphModified(boolean modified) {
    	jgraphModified = modified;
		refreshTitle();
    }
    
    /**
     * Returns the current modified status of the underlying jgraph.
     * @see #setJGraphModified(boolean)
     */
    protected boolean isJGraphModified() {
        return jgraphModified;
    }

    /**
     * Sets the name of the editor model.
     * The name may be <tt>null</tt> if the model is to be anonymous.
     * @param name new name for the editor model
     * @see EditorJModel#setName(String)
     */
    protected void setModelName(String name) {
        if (getModel() != null) {
            getModel().setName(name);
            refreshTitle();
        }
    }

    /**
     * Returns the current name of the editor model.
     * The name may be <tt>null</tt> if the model is anonymous.
     * @see EditorJModel#getName()
     */
    protected String getModelName() {
        if (getModel() != null) {
            return getModel().getName();
        } else {
            return null;
        }
    }
    
    /**
     * Sets the name of the graph in the title bar. If the indicated name is <tt>null</tt>, a
     * {@link #NEW_GRAPH_NAME} is used.
     */
    protected void refreshTitle() {
        String modelName = getModelName();
        String title = (jgraphModified ? MODIFIED_INDICATOR: "") + (modelName == null ? NEW_GRAPH_NAME : modelName) + " - " + EDITOR_NAME;
        Component window = getRootComponent();
        if (window instanceof JFrame) {
            ((JFrame) window).setTitle(title);        
        } else if (window instanceof JDialog) {
            ((JDialog) window).setTitle(title);                    
        }
    }

    /**
     * Returns the top level component of the graph panel in the containmeint hierarchy.
     */
    protected Component getRootComponent() {
        Component component = jGraphPanel;
        while (component != null && !(component instanceof JFrame || component instanceof JDialog)) {
            component = component.getParent();
        }
        return component;
    }
    
    /**
     * Creates and returns the menu bar. Requires the actions to have been initialized first.
     */
    protected JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        // file menu, only if the component is not auxiliary
        if (! isAuxiliary()) {
        JMenu fileMenu = new JMenu(Options.FILE_MENU_NAME);
        menuBar.add(fileMenu);
        fileMenu.add(newAction);
        fileMenu.add(openAction);
        fileMenu.addSeparator();
        fileMenu.add(saveAction);
        fileMenu.add(exportAction);
        fileMenu.addSeparator();
        fileMenu.add(quitAction);
        }

        // edit menu
        JMenu editMenu = new JMenu(Options.EDIT_MENU_NAME);
        menuBar.add(editMenu);
        editMenu.add(undoAction);
        editMenu.add(redoAction);
        editMenu.addSeparator();
        editMenu.add(cutAction);
        editMenu.add(copyAction);
        editMenu.add(pasteAction);
        editMenu.add(deleteAction);
        editMenu.addSeparator();
        editMenu.add(selectModeAction);
        editMenu.add(nodeModeAction);
        editMenu.add(edgeModeAction);
        jgraph.fillOutEditMenu(editMenu.getPopupMenu());

        // display menu
        JMenu displayMenu = new JMenu(Options.DISPLAY_MENU_NAME);
        menuBar.add(displayMenu);
        jgraph.fillOutDisplayMenu(displayMenu.getPopupMenu());
        displayMenu.addSeparator();
        displayMenu.add(jGraphPanel.getViewLabelListItem());

        // options menu
        JMenu optionsMenu = new JMenu(Options.OPTIONS_MENU_NAME);
        menuBar.add(optionsMenu);
        optionsMenu.add(options.getItem(IS_ATTRIBUTED_OPTION));

        return menuBar;
    }

    /**
     * Creates and returns the tool bar. Requires the actions to have been initialized.
     */
    protected JToolBar createToolBar() {
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        if (isAuxiliary()) {
            toolbar.add(closeAction);
        } else {
            toolbar.add(newAction);
            toolbar.add(openAction);
            toolbar.add(saveAction);
        }
        toolbar.add(getRulePreviewAction());

        // Mode block
        toolbar.addSeparator();

        toolbar.add(getSelectModeButton());        
        toolbar.add(getNodeModeButton());
        toolbar.add(getEdgeModeButton());


        // Undo Block
        toolbar.addSeparator();
        toolbar.add(undoAction);
        toolbar.add(redoAction);

        // Edit Block
        toolbar.addSeparator();
        toolbar.add(copyAction);
        toolbar.add(pasteAction);
        toolbar.add(cutAction);
        toolbar.add(deleteAction);
        return toolbar;
    }

    /** Returns a rule properties object based on the current options setting. */
    protected RuleProperties getRuleProperties() {
    	return RuleProperties.getInstance(getOptions().getValue(IS_ATTRIBUTED_OPTION));
    }
    
	/**
	 * Returns the group of editing mode buttons, lazily creating it first.
	 */
	private ButtonGroup getModeButtonGroup() {
		if (modeButtonGroup == null) {
			modeButtonGroup = new ButtonGroup();
			modeButtonGroup.add(getSelectModeButton());
			modeButtonGroup.add(getNodeModeButton());
			modeButtonGroup.add(getEdgeModeButton());
		}
		return modeButtonGroup;
	}

	/**
	 * Returns the button for setting edge editing mode, lazily creating it first.
	 */
	private JToggleButton getEdgeModeButton() {
		if (edgeModeButton == null) {
			edgeModeButton = new JToggleButton(edgeModeAction);
			edgeModeButton.setText(null);
			edgeModeButton.setToolTipText(Options.EDGE_MODE_NAME);
		}
		return edgeModeButton;
	}

	/**
	 * Returns the button for setting node editing mode, lazily creating it first.
	 */
	private JToggleButton getNodeModeButton() {
		if (nodeModeButton == null) {
			nodeModeButton = new JToggleButton(nodeModeAction);
			nodeModeButton.setText(null);
			nodeModeButton.setToolTipText(Options.NODE_MODE_NAME);
		}
		return nodeModeButton;
	}

	/**
	 * Returns the button for setting selection mode, lazily creating it first.
	 */
	private JToggleButton getSelectModeButton() {
		if (selectModeButton == null) {
			selectModeButton = new JToggleButton(selectModeAction);
			selectModeButton.setText(null);
			selectModeButton.setToolTipText(Options.SELECT_MODE_NAME);
			selectModeButton.doClick();
		}
		return selectModeButton;
	}

    /** 
     * Updates the Undo/Redo Button State based on Undo Manager.
     * Also sets {@link #jgraphModified} if no more undos are available.
     */
    protected void updateHistoryButtons() {
        // The View Argument Defines the Context
        undoAction.setEnabled(undoManager.canUndo());
        redoAction.setEnabled(undoManager.canRedo());
        setJGraphModified(undoManager.canUndo());
    }

    /**
     * Activates the appropriate mode button (select, node or edge), based on a given (mode) action.
     * @param forAction the mode action for which the corresponding button is to be activated
     */
    protected void updateModeButtons(Action forAction) {
        Enumeration<AbstractButton> modeButtonEnum = getModeButtonGroup().getElements();
        while (modeButtonEnum.hasMoreElements()) {
            JToggleButton button = (JToggleButton) modeButtonEnum.nextElement();
            if (button.getAction() == forAction) {
                button.setSelected(true);
            }
        }
    }
    
    /** Updates the status bar with information about the currently edited graph. */
    protected void updateStatus() {
        int elementCount = getModel().getRootCount() - getModel().getHiddenCount();
        statusBar.setText(""+elementCount+" visible elements");
    }
    
    /** Sets the property whether all inserted cells are automatically selected. */
    protected void setSelectInsertedCells(boolean select) {
        jgraph.getGraphLayoutCache().setSelectsAllInsertedCells(select);  
    }
    
    /** Returns the current property whether all inserted cells are automatically selected. */
    protected boolean isSelectInsertedCells() {
        return jgraph.getGraphLayoutCache().isSelectsAllInsertedCells();          
    }

    /** Undoes the last registered change to the Model or the View. */
    protected void undoLastEdit() {
        try {
            setSelectInsertedCells(false);
            undoManager.undo(jgraph.getGraphLayoutCache());
            setSelectInsertedCells(true);
        } catch (Exception ex) {
            System.err.println(ex);
        } finally {
            updateHistoryButtons();
        }
    }

    /** Redoes the latest undone change to the Model or the View. */
    protected void redoLastEdit() {
        try {
            setSelectInsertedCells(false);
            undoManager.redo(jgraph.getGraphLayoutCache());
            setSelectInsertedCells(true);
        } catch (Exception ex) {
            System.err.println(ex);
        } finally {
            updateHistoryButtons();
        }
    }

    /** Creates and displays an {@link ErrorDialog} with a given message and exception. */
    private void showErrorDialog(String message, Exception exc) {
        new ErrorDialog(jGraphPanel, message, exc).setVisible(true);
    }

    /** Creates and shows a confirmation dialog for abandoning the currently edited graph. */
    private boolean showAbandonDialog() {
        if (isJGraphModified()) {
            int res = JOptionPane.showConfirmDialog(jGraphPanel,
                "Abandon changes in current graph?",
                null,
                JOptionPane.OK_CANCEL_OPTION);
            return res == JOptionPane.OK_OPTION;
        } else
            return true;
    }

	/**
     * Sets the {@link #ruleFactory} with the given object.
     * @param ruleFactory the new rule factory
     */
    protected void setRuleFactory(RuleFactory ruleFactory) {
    	this.ruleFactory = ruleFactory;
    }

    /**
     * Returns the rule factory.
     * @return the {@link #ruleFactory}-value
     */
    protected RuleFactory getRuleFactory() {
    	if (ruleFactory == null) {
    		ruleFactory = DefaultRuleFactory.getInstance();
    	}
    	return ruleFactory;
    }
    
    /** Returns the rule preview action, lazily creating it first. */
    Action getRulePreviewAction() {
    	if (rulePreviewAction == null) {
    		rulePreviewAction = new RulePreviewAction();
    	}
    	return rulePreviewAction;
    }

    /**
     * Returns the options object associated with the simulator.
     */
    Options getOptions() {
    	// lazily creates the options 
    	if (options == null) {
    		options = new Options();
        	options.add(Options.IS_ATTRIBUTED_OPTION);
    	}
    	return options;
    }
    
    /**
     * The options object of this simulator.
     */
    private Options options;

    /** The jgraph instance used in this editor. */
    private final EditorJGraph jgraph;

    /**
     * Rule factory used for previewing the graph as a rule.
     */
    private RuleFactory ruleFactory;

    /**
     * Indicates if the editor is an auxiliary component.
     */
    private final boolean auxiliary;
    
    /** The tool bar of this editor. */
    private JToolBar editorToolBar;
    
    /** The jgraph panel used in this editor. */
    private final JGraphPanel<EditorJGraph> jGraphPanel;
    
    /** Status bar of the editor. */
    private final JLabel statusBar = new JLabel();

    /** Checkbox to indicate that saving rules should be preceded by a preview. */
    private JCheckBox confirmPreviewCheckBox;

    /** Indicates whether jgraph has been modified since the last save. */
    private boolean jgraphModified;

    /** The undo manager of the editor. */
    private transient GraphUndoManager undoManager;

    /** Currently edited file. */
    private File currentFile;

    /** Current directory of file choosers. */
    private File currentDir = new File(Groove.WORKING_DIR);
    /**
     * The GXL converter used for marshalling and unmarshalling layouted graphs.
     */
    private final Xml<Graph> layoutGxl = new LayedOutXml();

    /**
     * File chooser for graph opening.
     */
    private JFileChooser graphOpenChooser;

    /**
     * File chooser for graph saving.
     */
    private JFileChooser graphSaveChooser;

    /**
     * File chooser for export actions.
     */
    private JFileChooser exportChooser;

    /**
     * Extension filter used for exporting the graph in fsm format.
     */
    private final ExtensionFilter fsmFilter = Groove.createFsmFilter();

    /**
     * Extension filter used for exporting the graph in jpeg format.
     */
    private final ExtensionFilter jpgFilter = new ExtensionFilter("JPEG image files",
            Groove.JPG_EXTENSION);

    /**
     * Extension filter used for exporting the graph in PNG format.
     */
    private final ExtensionFilter pngFilter = new ExtensionFilter("PNG files",
            Groove.PNG_EXTENSION);

    /**
     * Extension filter used for exporting the graph in EPS format.
     */
    private final ExtensionFilter epsFilter = new ExtensionFilter("EPS files",
            Groove.EPS_EXTENSION);

    /**
     * Extension filter for state files.
     */
    private final ExtensionFilter stateFilter = Groove.createStateFilter();

    /**
     * Extension filter for rule files.
     */
    private final ExtensionFilter ruleFilter = Groove.createRuleFilter();

    /**
     * Extension filter used for exporting the LTS in jpeg format.
     */
    private final ExtensionFilter gxlFilter = Groove.createGxlFilter();
    
    /**
     * Extension filter for all known kinds of graph files.
     */
    private final ExtensionFilter graphFilter = new ExtensionFilter("Graph files", "") {
        @Override
        public boolean accept(File file) {
            return isAcceptDirectories() && file.isDirectory()  
            		|| file.getName().endsWith(Groove.GXL_EXTENSION)
                    || file.getName().endsWith(Groove.RULE_EXTENSION)
                    || file.getName().endsWith(Groove.STATE_EXTENSION);
        }

        @Override
        public String getDescription() {
            return "Graph files (*" + Groove.GXL_EXTENSION + ", *" + Groove.RULE_EXTENSION
                    + ", *" + Groove.STATE_EXTENSION + ")";
        }
        
        @Override
        public boolean acceptExtension(File file) {
            return false;
        }
        
        @Override
        public String stripExtension(String fileName) {
            File file = new File(fileName);
            if (gxlFilter.acceptExtension(file)) {
                return gxlFilter.stripExtension(fileName);
            } else if (stateFilter.acceptExtension(file)) {
                return stateFilter.stripExtension(fileName);
            } else if (ruleFilter.acceptExtension(file)) {
                return ruleFilter.stripExtension(fileName);
            } else {
                return fileName;
            }
        }
    };

    /** Action to undo the last edit. */
    private Action undoAction;
    /** Action to redo the last (undone) edit. */
    private Action redoAction;
    /** Action to delete the selected elements. */
    private Action deleteAction;
    /** Action to cut the selected elements. */
    private Action cutAction;
    /** Action to copy the selected elements. */
    private Action copyAction;
    /** Action to paste the previously cut or copied elements. */
    private Action pasteAction;
    /** Action to create a rule preview dialog. */
    private Action rulePreviewAction;
    /** Action to save the current graph. */
    private Action saveAction;
    /** Action to export the current graph in an image format. */
    private Action exportAction;
    /** Action to open a new graph for editing. */
    private Action openAction;
    /** Action to start an empty graph for editing. */
    private Action newAction;

    /** Action to close the editor. Only if the editor is auxiliary. */
    protected Action closeAction;
    /** Action to quit the editor. Only if the editor is not auxiliary. */
    protected Action quitAction;
    /** Action to set the editor to selection mode. */
    protected Action selectModeAction;
    /** Action to set the editor to node editing mode. */
    protected Action nodeModeAction;
    /** Action to set the editor to edge editing mode. */
    protected Action edgeModeAction;

    /** Button for setting edge editing mode. */
    private transient JToggleButton edgeModeButton;
    /** Button for setting node editing mode. */
    private transient JToggleButton nodeModeButton;
    /** Button for setting selection mode. */
    private transient JToggleButton selectModeButton;
    /** Collection of editing mode buttons. */
    private ButtonGroup modeButtonGroup;
}