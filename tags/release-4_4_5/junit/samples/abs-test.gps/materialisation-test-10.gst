<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="materialisation-test-10">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n1">
            <attr name="layout">
                <string>407 360 36 48</string>
            </attr>
        </node>
        <node id="n6">
            <attr name="layout">
                <string>131 133 36 48</string>
            </attr>
        </node>
        <node id="n0">
            <attr name="layout">
                <string>131 354 36 48</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>281 358 36 48</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>126 191 36 48</string>
            </attr>
        </node>
        <node id="n5">
            <attr name="layout">
                <string>286 273 36 48</string>
            </attr>
        </node>
        <node id="n7">
            <attr name="layout">
                <string>289 139 36 48</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>284 197 36 48</string>
            </attr>
        </node>
        <edge to="n1" from="n2">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge to="n3" from="n4">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n4" from="n4">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n7" from="n7">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge to="n5" from="n0">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n2" from="n0">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n6" from="n6">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n2" from="n2">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge to="n7" from="n6">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge to="n5" from="n5">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
    </graph>
</gxl>
