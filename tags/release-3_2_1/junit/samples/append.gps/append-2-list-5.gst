<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n104"/>
        <node id="n94"/>
        <node id="n102"/>
        <node id="n99"/>
        <node id="n97"/>
        <node id="n103"/>
        <node id="n93"/>
        <node id="n98"/>
        <node id="n95"/>
        <node id="n100"/>
        <node id="n92"/>
        <node id="n96"/>
        <node id="n101"/>
        <node id="n90"/>
        <node id="n91"/>
        <edge from="n96" to="n100">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n97" to="n104">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n97" to="n95">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n95" to="n99">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n91" to="n91">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n103" to="n93">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n101" to="n101">
            <attr name="label">
                <string>6</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n92" to="n103">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge from="n90" to="n97">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n96" to="n92">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n103" to="n90">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n95" to="n101">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n93" to="n93">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n94" to="n102">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n94" to="n103">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n96" to="n103">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n98" to="n98">
            <attr name="label">
                <string>7</string>
            </attr>
        </edge>
        <edge from="n99" to="n98">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n90" to="n91">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n94" to="n92">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n102" to="n102">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
    </graph>
</gxl>
