/* $Id: GraphConditionOutcome.java,v 1.3 2007-10-05 08:31:41 rensink Exp $ */
package groove.trans;


/**
 * A specialised interface that models the outcome of a graph condition.
 * @author Arend Rensink
 * @version $Revision $
 */
@Deprecated
public interface GraphConditionOutcome extends GraphTestOutcome<Matching,GraphCondition> {
	// nothing but a specialised interface
}
