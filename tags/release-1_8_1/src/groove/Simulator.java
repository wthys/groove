// GROOVE: GRaphs for Object Oriented VErification
// Copyright 2003--2007 University of Twente
 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// http://www.apache.org/licenses/LICENSE-2.0 
 
// Unless required by applicable law or agreed to in writing, 
// software distributed under the License is distributed on an 
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
// either express or implied. See the License for the specific 
// language governing permissions and limitations under the License.
/*
 * $Id: Simulator.java,v 1.3 2007-05-09 22:53:37 rensink Exp $
 */
package groove;


/**
 * Wrapper class for the simulator
 * @see groove.gui.Simulator
 * @author Arend Rensink
 * @version $Revision: 1.3 $
 */
public class Simulator {
    /**
     * Main method.
     * @param args list of command-line arguments
     */
    static public void main(String[] args) {
    	groove.gui.Simulator.main(args);
    }
}
