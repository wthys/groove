package groove.explore.result;


/** Never accepts. To be used with {@link EmptyResult}.
 */
public class EmptyAcceptor extends Acceptor<Object> {
}
