<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n18"/>
        <node id="n19"/>
        <node id="n16"/>
        <node id="n15"/>
        <node id="n10"/>
        <node id="n3"/>
        <node id="n2"/>
        <node id="n9"/>
        <node id="n7"/>
        <node id="n11"/>
        <node id="n12"/>
        <node id="n8"/>
        <node id="n6"/>
        <node id="n14"/>
        <node id="n20"/>
        <node id="n4"/>
        <node id="n21"/>
        <node id="n5"/>
        <node id="n1"/>
        <node id="n22"/>
        <node id="n17"/>
        <node id="n0"/>
        <node id="n13"/>
        <edge from="n3" to="n13">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n7" to="n10">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n13" to="n5">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n18" to="n13">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n11" to="n12">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n5" to="n16">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n3" to="n4">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n0" to="n13">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge from="n9" to="n22">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n18" to="n4">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>10</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n5" to="n14">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n15" to="n7">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n4" to="n13">
            <attr name="label">
                <string>list</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>root</string>
            </attr>
        </edge>
        <edge from="n9" to="n11">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n0" to="n4">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>11</string>
            </attr>
        </edge>
        <edge from="n17" to="n4">
            <attr name="label">
                <string>caller</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>'-20'</string>
            </attr>
        </edge>
        <edge from="n0" to="n2">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>15</string>
            </attr>
        </edge>
        <edge from="n3" to="n20">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n13" to="n1">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n8" to="n2">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n17" to="n13">
            <attr name="label">
                <string>this</string>
            </attr>
        </edge>
        <edge from="n18" to="n21">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n14" to="n8">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n17" to="n20">
            <attr name="label">
                <string>x</string>
            </attr>
        </edge>
        <edge from="n11" to="n15">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n15" to="n19">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>'-1'</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>control</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n8" to="n9">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>append</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n14" to="n6">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
    </graph>
</gxl>
