// $ANTLR 2.7.6 (2005-12-22): "gcl.g" -> "GCLParser.java"$

package groove.control;

public interface GCLBuilderTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int ALAP = 4;
	int UNTIL = 5;
	int TRY = 6;
	int ELSE = 7;
	int DO = 8;
	int PROGRAM = 9;
	int LCURLY = 10;
	int RCURLY = 11;
	int SEMICOLON = 12;
	int OR = 13;
	int IDENTIFIER = 14;
	int LPAREN = 15;
	int RPAREN = 16;
}
