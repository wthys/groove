<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="start">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n1">
            <attr name="layout">
                <string>67 40 55 32</string>
            </attr>
        </node>
        <node id="n0">
            <attr name="layout">
                <string>70 110 52 64</string>
            </attr>
        </node>
        <edge to="n0" from="n1">
            <attr name="label">
                <string>h</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>type:E</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>e</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
    </graph>
</gxl>
