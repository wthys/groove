<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="addEdgesToMergedNodes-start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n137734"/>
        <node id="n137735"/>
        <node id="n137737"/>
        <node id="n137738"/>
        <node id="n137736"/>
        <edge from="n137735" to="n137735">
            <attr name="label">
                <string>rem:</string>
            </attr>
        </edge>
        <edge from="n137737" to="n137737">
            <attr name="label">
                <string>rem:</string>
            </attr>
        </edge>
        <edge from="n137736" to="n137736">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n137735" to="n137736">
            <attr name="label">
                <string>first</string>
            </attr>
        </edge>
        <edge from="n137737" to="n137738">
            <attr name="label">
                <string>trigger</string>
            </attr>
        </edge>
        <edge from="n137734" to="n137734">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n137735" to="n137735">
            <attr name="label">
                <string>nodes that will be merged</string>
            </attr>
        </edge>
        <edge from="n137735" to="n137734">
            <attr name="label">
                <string>second</string>
            </attr>
        </edge>
        <edge from="n137737" to="n137737">
            <attr name="label">
                <string>trigger for the universal</string>
            </attr>
        </edge>
        <edge from="n137738" to="n137738">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
    </graph>
</gxl>
