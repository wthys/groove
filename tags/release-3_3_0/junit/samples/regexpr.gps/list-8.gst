<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n254"/>
        <node id="n263"/>
        <node id="n267"/>
        <node id="n253"/>
        <node id="n265"/>
        <node id="n264"/>
        <node id="n262"/>
        <node id="n259"/>
        <node id="n268"/>
        <node id="n255"/>
        <node id="n260"/>
        <node id="n261"/>
        <node id="n257"/>
        <node id="n266"/>
        <node id="n258"/>
        <node id="n252"/>
        <node id="n256"/>
        <edge from="n254" to="n254">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n263" to="n265">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n263" to="n252">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n267" to="n267">
            <attr name="label">
                <string>8</string>
            </attr>
        </edge>
        <edge from="n253" to="n264">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n253" to="n261">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n265" to="n258">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n265" to="n254">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n264" to="n264">
            <attr name="label">
                <string>4</string>
            </attr>
        </edge>
        <edge from="n262" to="n262">
            <attr name="label">
                <string>5</string>
            </attr>
        </edge>
        <edge from="n259" to="n259">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge from="n268" to="n267">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n255" to="n257">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n255" to="n260">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n260" to="n256">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n260" to="n268">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n261" to="n262">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n261" to="n255">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n257" to="n257">
            <attr name="label">
                <string>6</string>
            </attr>
        </edge>
        <edge from="n266" to="n266">
            <attr name="label">
                <string>List</string>
            </attr>
        </edge>
        <edge from="n266" to="n263">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n258" to="n253">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n258" to="n259">
            <attr name="label">
                <string>val</string>
            </attr>
        </edge>
        <edge from="n252" to="n252">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n256" to="n256">
            <attr name="label">
                <string>8</string>
            </attr>
        </edge>
    </graph>
</gxl>
