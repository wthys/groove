<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="host-6">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n5">
            <attr name="layout">
                <string>401 171 27 32</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>302 291 28 32</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>300 172 28 32</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>300 228 28 32</string>
            </attr>
        </node>
        <node id="n1">
            <attr name="layout">
                <string>215 230 24 32</string>
            </attr>
        </node>
        <node id="n0">
            <attr name="layout">
                <string>135 232 27 32</string>
            </attr>
        </node>
        <edge to="n5" from="n5">
            <attr name="label">
                <string>type:P</string>
            </attr>
        </edge>
        <edge to="n3" from="n5">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge to="n4" from="n4">
            <attr name="label">
                <string>type:L</string>
            </attr>
        </edge>
        <edge to="n1" from="n4">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>type:L</string>
            </attr>
        </edge>
        <edge to="n1" from="n3">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n2" from="n2">
            <attr name="label">
                <string>type:L</string>
            </attr>
        </edge>
        <edge to="n1" from="n2">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>type:I</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>type:F</string>
            </attr>
        </edge>
        <edge to="n1" from="n0">
            <attr name="label">
                <string>o</string>
            </attr>
        </edge>
    </graph>
</gxl>
