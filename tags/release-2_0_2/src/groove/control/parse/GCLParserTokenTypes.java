// $ANTLR 2.7.6 (2005-12-22): "gcl.g" -> "GCLParser.java"$

package groove.control.parse;
import groove.control.*;

public interface GCLParserTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int ALAP = 4;
	int WHILE = 5;
	int TRY = 6;
	int ELSE = 7;
	int DO = 8;
	int IF = 9;
	int CHOICE = 10;
	int OR = 11;
	int PROC = 12;
	int PROCUSE = 13;
	int PROGRAM = 14;
	int BLOCK = 15;
	int TRUE = 16;
	int LCURLY = 17;
	int RCURLY = 18;
	int IDENTIFIER = 19;
	int LPAREN = 20;
	int RPAREN = 21;
	int SEMICOLON = 22;
	int PLUS = 23;
	int STAR = 24;
	int SHARP = 25;
	int AND = 26;
	int COMMA = 27;
	int DOT = 28;
	int NOT = 29;
	int RSQUARE = 30;
	int DIGIT = 31;
	int LETTER = 32;
	int NEWLINE = 33;
	int WS = 34;
	int SPECIAL = 35;
}
