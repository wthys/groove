<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="testgraph" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n56894"/>
        <node id="n56895"/>
        <node id="n56896"/>
        <node id="n56897"/>
        <node id="n56898"/>
        <node id="n56899"/>
        <node id="n56900"/>
        <edge from="n56900" to="n56896">
            <attr name="label">
                <string>r,[1,3],[3]</string>
            </attr>
        </edge>
        <edge from="n56898" to="n56898">
            <attr name="label">
                <string>s2</string>
            </attr>
        </edge>
        <edge from="n56897" to="n56897">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n56896" to="n56894">
            <attr name="label">
                <string>r,[1,2],[2]</string>
            </attr>
        </edge>
        <edge from="n56896" to="n56896">
            <attr name="label">
                <string>s1</string>
            </attr>
        </edge>
        <edge from="n56900" to="n56900">
            <attr name="label">
                <string>s0</string>
            </attr>
        </edge>
        <edge from="n56898" to="n56894">
            <attr name="label">
                <string>r,[1,3],[3]</string>
            </attr>
        </edge>
        <edge from="n56899" to="n56899">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n56900" to="n56898">
            <attr name="label">
                <string>r,[1,2],[2]</string>
            </attr>
        </edge>
        <edge from="n56894" to="n56894">
            <attr name="label">
                <string>s3</string>
            </attr>
        </edge>
        <edge from="n56895" to="n56895">
            <attr name="label">
                <string>C</string>
            </attr>
        </edge>
    </graph>
</gxl>
