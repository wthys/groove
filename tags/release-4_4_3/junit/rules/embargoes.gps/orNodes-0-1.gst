<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph edgemode="directed" edgeids="false" role="graph" id="orNodes-0-1">
        <node id="n2">
            <attr name="layout">
                <string>229 217 34 61</string>
            </attr>
        </node>
        <node id="n0">
            <attr name="layout">
                <string>228 136 34 61</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>235 42 34 61</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>67 116 34 46</string>
            </attr>
        </node>
        <node id="n6">
            <attr name="layout">
                <string>382 143 34 46</string>
            </attr>
        </node>
        <node id="n1">
            <attr name="layout">
                <string>234 308 46 76</string>
            </attr>
        </node>
        <edge to="n6" from="n2">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n6" from="n3">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>flag:mark</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>flag:3</string>
            </attr>
        </edge>
        <edge to="n3" from="n3">
            <attr name="label">
                <string>flag:0</string>
            </attr>
        </edge>
        <edge to="n4" from="n0">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>flag:2</string>
            </attr>
        </edge>
        <edge to="n0" from="n0">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n4" from="n4">
            <attr name="label">
                <string>type:B</string>
            </attr>
        </edge>
        <edge to="n2" from="n2">
            <attr name="label">
                <string>flag:1</string>
            </attr>
        </edge>
        <edge to="n4" from="n3">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge to="n1" from="n1">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n2" from="n2">
            <attr name="label">
                <string>type:A</string>
            </attr>
        </edge>
        <edge to="n6" from="n6">
            <attr name="label">
                <string>type:C</string>
            </attr>
        </edge>
    </graph>
</gxl>
