/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.java;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.itextpdf.text.pdf.codec.Base64;

/**
 * Class to wrap a native Java Object.
 * @author Wim Thys
 * @version $Revision $
 */
public class NativeObject {
    private Object value;

    /** The one and only NativeObject representing <code>null</code>. */
    public static final NativeObject NULL = new NativeObject();

    /**
     * Construct a NativeObject with value <code>null</code>.
     */
    private NativeObject() {
        this.value = null;
    }

    /**
     * Construct a NativeObject with the specified value.
     * @param value The value to initialise with.
     */
    public NativeObject(Object value) {
        if (value instanceof Serializable) {
            this.value = value;
        } else {
            throw new IllegalArgumentException("Object is not serializable");
        }
    }

    /**
     * Get the value contained within which can be <code>null</code>.
     * @return The contained value.
     */
    public Object getValue() {
        return this.value;
    }

    /**
     * Set the new value for this NativeObject. Overwrites the previous value if it was non-<code>null</code>.
     * @param value The new value.
     */
    public void setValue(Object value) {
        if (value instanceof Serializable) {
            this.value = value;
        } else {
            throw new IllegalArgumentException("Object is not serializable");
        }
    }

    /**
     * @return Whether we contain a <code>null</code> value. 
     */
    public boolean isNull() {
        return this.value == null;
    }

    /**
     * Get the class of the contained Object. If there is no such Object, returns <code>null</code>.
     * @return The class of the contained Object or <code>null</code> if it does not exist.
     */
    public Class<?> getInnerClass() {
        if (isNull()) {
            return null;
        } else {
            return this.value.getClass();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof NativeObject) {
            NativeObject nobj = (NativeObject) obj;
            boolean ans = this.value == nobj.value;
            return ans;
        } else {
            boolean ans = this.value == obj;
            return ans;
        }
    }

    @Override
    public int hashCode() {
        if (isNull()) {
            return 0;
        } else {
            return this.value.hashCode();
        }
    }

    @Override
    public String toString() {
        if (isNull()) {
            return "null";
        } else {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream out;
                out = new ObjectOutputStream(baos);
                out.writeObject(this.value);
                byte[] data = baos.toByteArray();
                String encoded =
                    Base64.encodeBytes(data, Base64.DONT_BREAK_LINES);
                return String.format("[%s@%s]",
                    this.value.getClass().getName(), encoded);
            } catch (IOException e) {
                System.out.println("something happened so i got a ioexception: "
                    + e);
                return "null";
            }
        }
    }

    /**
     * Whether the string could represent a NativeObject.
     * @param value The string value to check.
     * @return Whether we have a NativeObject representation.
     */
    public static boolean isRepr(String value) {
        try {
            return NativeObject.fromString(value) != null;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    /**
     * Parses a NativeObject from a string.
     * @param value The string to parse.
     * @return The NativeObject.
     */
    public static NativeObject fromString(String value) {
        if (value.equals("null")) {
            return NULL;
        } else if (value.startsWith("[") && value.endsWith("]")
            && value.lastIndexOf('[') == 0
            && value.indexOf(']') == value.length() - 1) {
            String[] parts = value.substring(1, value.length() - 1).split("@");
            if (parts.length == 2) {
                try {
                    byte[] data =
                        Base64.decode(parts[1], Base64.DONT_BREAK_LINES);
                    ByteArrayInputStream bais = new ByteArrayInputStream(data);
                    ObjectInputStream in = new ObjectInputStream(bais);
                    Object obj = in.readObject();
                    if (obj == null) {
                        throw new IllegalArgumentException(
                            "Invalid native object notation");
                    }
                    return new NativeObject(obj);
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                } catch (ClassNotFoundException e) {
                    throw new IllegalArgumentException(
                        "Serialized class could not be found in classpath");
                }
            }
        }
        return null;
    }
}
