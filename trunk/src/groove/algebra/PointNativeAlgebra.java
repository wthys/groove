/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.algebra;

import java.util.List;

/** Implementation of natives with a singleton value.
 * @author Wim Thys <wim.thys@zardof.be>
 * @version $Revision $
 */
public class PointNativeAlgebra extends
        AbstractNativeAlgebra<Object,Object,Object,Object,Object> {

    /** Name of this algebra. */
    public static final String NAME = "pnative";
    /** Singleton instance of this algebra. */
    public static final PointNativeAlgebra instance = new PointNativeAlgebra();
    private static final String singleNative =
        SignatureKind.NATIVE.getDefaultValue();
    private static final String singleBool =
        SignatureKind.BOOL.getDefaultValue();
    private static final String singleString =
        SignatureKind.STRING.getDefaultValue();
    private static final String singleInt = SignatureKind.INT.getDefaultValue();
    private static final String singleReal =
        SignatureKind.REAL.getDefaultValue();

    private PointNativeAlgebra() {
        //empty
    }

    @Override
    public Object getValueFromSymbol(String constant) {
        return singleNative;
    }

    @Override
    public String getSymbol(Object value) {
        return null;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public AlgebraFamily getFamily() {
        return AlgebraFamily.POINT;
    }

    @Override
    public Object eq(Object a, Object b) {
        return singleBool;
    }

    @Override
    public Object same(Object a, Object b) {
        return singleBool;
    }

    @Override
    public Object toString(Object a) {
        return singleString;
    }

    @Override
    public Object n2s(Object a) {
        return singleString;
    }

    @Override
    public Object n2r(Object a) {
        return singleReal;
    }

    @Override
    public Object n2i(Object a) {
        return singleInt;
    }

    @Override
    public Object n2b(Object a) {
        return singleBool;
    }

    @Override
    public Object s2n(Object a) {
        return singleNative;
    }

    @Override
    public Object i2n(Object a) {
        return singleNative;
    }

    @Override
    public Object r2n(Object a) {
        return singleNative;
    }

    @Override
    public Object b2n(Object a) {
        return singleNative;
    }

    @Override
    protected Object toValue(Object value) {
        return value;
    }

    @Override
    public Operation getOperation(Operator operator) {
        if (operator instanceof MethodCallOperator) {
            return new Operation() {

                @Override
                public Object apply(List<Object> args)
                    throws IllegalArgumentException {
                    //TODO wimthys: add syntax checking stuff here?
                    return null;
                }

                @Override
                public String getName() {
                    return "return";
                }

                @Override
                public int getArity() {
                    return 0;
                }

                @Override
                public Algebra<?> getAlgebra() {
                    return PointNativeAlgebra.instance;
                }

                @Override
                public Algebra<?> getResultAlgebra() {
                    return PointNativeAlgebra.instance;
                }

            };
        } else {
            //TODO wimthys: error state?
            return null;
        }
    }
}
