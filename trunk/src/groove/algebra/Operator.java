/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.algebra;

import java.util.List;

/**
 * Interface for an operator.
 * @author Wim Thys
 * @version $Revision $
 */
public interface Operator {

    /** Returns the signature to which this operator belongs. */
    public SignatureKind getSignature();

    /** Returns the name of the operator. */
    public String getName();

    /** Returns the number of parameters of this operator. */
    public int getArity();

    /** 
     * Returns the parameter type names of this operator.
     * The type names are actually the names of the defining signatures. 
     */
    public List<SignatureKind> getParamTypes();

    /** 
     * Returns the result type name of this operator.
     * The type name is actually the name of the defining signature.
     */
    public SignatureKind getResultType();

    /** Returns the name of the operator, preceded with its type prefix. */
    public String getTypedName();

    /** Returns the infix symbol of this operator, or {@code null} if it has none. */
    public String getSymbol();

    /** Returns the priority of this operator. */
    public Precedence getPrecedence();

}