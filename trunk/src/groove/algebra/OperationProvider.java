/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.algebra;


/**
 * Provides {@link Operation} object given an {@link Operator}.
 * @author Wim Thys <wim.thys@zardof.be>
 * @version $Revision $
 */
public interface OperationProvider {
    /**
     * Get the an associated {@link Operation} given the supplied {@link Operator}.
     * @param operator The {@link Operator} to take into account.
     * @return The associated {@link Operation} that performs the functionality of the {@link Operator}.
     */
    public Operation getOperation(Operator operator);
}
