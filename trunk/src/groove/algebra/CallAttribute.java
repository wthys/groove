/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.algebra;

import groove.grammar.model.FormatException;

/**
 * Attributes available for calling a method.
 * @author Wim Thys
 * @version $Revision $
 */
public enum CallAttribute {
    /** Name of the method. */
    METHOD("method"),
    /** Class of the method. */
    CLASS("class"),
    /** Object on which to act */
    OBJECT("object"),
    /** Return value of the method */
    RETURN("return");

    private CallAttribute(String value) {
        this.value = value;
    }

    /** Returns a call attribute by parsing a string one of the possible 
     * attribute names.
     * @param text the text to be parsed
     * @return The corresponding call attribute. Calling {@link #toString()}
     * will yield the value of {@code text}.
     * @throws FormatException if the input text was not correctly formatted, 
     * i.e. not one of the known attributes.
     */
    public static CallAttribute parse(String text) throws FormatException {
        for (CallAttribute ck : values()) {
            if (ck.value.equals(text)) {
                return ck;
            }
        }
        throw new FormatException("Invalid call attribute name %s", text);
    }

    @Override
    public String toString() {
        return this.value;
    }

    private final String value;
}
