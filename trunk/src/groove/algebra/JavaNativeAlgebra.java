/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.algebra;

import groove.java.NativeObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import com.jaxfront.core.util.MethodUtils;

/**
 * Implementation of native algebra.
 * @author Wim Thys <wim.thys@zardof.be>
 * @version $Revision $
 */
public class JavaNativeAlgebra extends
        AbstractNativeAlgebra<NativeObject,String,Boolean,Double,Integer> {
    /** The name of the algebra */
    public static final String NAME = "jnative";
    /** The singleton instance of this algebra */
    public static final JavaNativeAlgebra instance = new JavaNativeAlgebra();

    private JavaNativeAlgebra() {
        //empty
    }

    @Override
    public Object getValueFromSymbol(String constant) {
        if (!isValue(constant)) {
            return null;
        } else {
            NativeObject obj = NativeObject.fromString(constant);
            return obj;
        }
    }

    @Override
    public String getSymbol(Object value) {
        NativeObject nobj;
        if (value != null && value instanceof NativeObject) {
            nobj = (NativeObject) value;
        } else {
            nobj = new NativeObject(value);
        }
        return nobj.toString();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public AlgebraFamily getFamily() {
        return AlgebraFamily.DEFAULT;
    }

    @Override
    public Boolean eq(NativeObject a, NativeObject b) {
        Object aa = a.getValue();
        Object bb = b.getValue();
        if (aa == null && bb == null) {
            return true;
        } else if (aa != null) {
            return aa.equals(bb);
        } else {
            return bb.equals(aa);
        }
    }

    @Override
    public Boolean same(NativeObject a, NativeObject b) {
        return a.equals(b);
    }

    @Override
    public String toString(NativeObject a) {
        if (a.isNull()) {
            return "null";
        } else {
            return a.getValue().toString();
        }
    }

    @Override
    public String n2s(NativeObject a) {
        if (a.isNull()) {
            throw new IllegalArgumentException("null is not a string");
        } else {
            Object obj = a.getValue();
            if (obj instanceof String) {
                return (String) obj;
            } else {
                throw new IllegalArgumentException(String.format(
                    "%s is not a string", obj));
            }
        }
    }

    @Override
    public Double n2r(NativeObject a) {
        if (a.isNull()) {
            throw new IllegalArgumentException("null is not a real number");
        } else {
            Object obj = a.getValue();
            if (obj instanceof Double) {
                return (Double) obj;
            } else {
                throw new IllegalArgumentException(String.format(
                    "%s is not a real number", obj));
            }
        }
    }

    @Override
    public Integer n2i(NativeObject a) {
        if (a.isNull()) {
            throw new IllegalArgumentException("null is not an integer");
        } else {
            Object obj = a.getValue();
            if (obj instanceof Integer) {
                return (Integer) obj;
            } else {
                throw new IllegalArgumentException(String.format(
                    "%s is not an integer", obj));
            }
        }
    }

    @Override
    public Boolean n2b(NativeObject a) {
        if (a.isNull()) {
            throw new IllegalArgumentException("null is not a boolean");
        } else {
            Object obj = a.getValue();
            if (obj instanceof Boolean) {
                return (Boolean) obj;
            } else {
                throw new IllegalArgumentException(String.format(
                    "%s is not a boolean", obj));
            }
        }
    }

    @Override
    protected NativeObject toValue(Object value) {
        return new NativeObject(value);
    }

    @Override
    public Operation getOperation(Operator operator) {
        assert operator != null;

        if (!(operator instanceof MethodCallOperator)) {
            throw new IllegalArgumentException(String.format(
                "Operator %s is not a method call", operator));
        }

        MethodCallOperator mco = (MethodCallOperator) operator;

        if (!mco.isValidMethod()) {
            throw new IllegalArgumentException(
                "Operator does not have valid attributes");
        }

        String method = mco.getName();
        Object object = mco.getMethodObject();
        Class<?> klass = mco.getMethodClass();

        Operation op = null;
        if (mco.isStaticCall()) {
            op = new Operation(klass, method);
        } else {
            op = new Operation(object, method);
        }

        return op;
    }

    private class Operation implements groove.algebra.Operation {

        private Object object;
        private String method;
        private Class<?> klass;

        public Operation(Object object, String method) {
            this.object = object;
            this.method = method;
            this.klass = object.getClass();
        }

        public Operation(Class<?> klass, String method) {
            this.object = null;
            this.method = method;
            this.klass = klass;
        }

        @Override
        public Object apply(List<Object> args) throws IllegalArgumentException {
            Class<?>[] partypes = new Class<?>[args.size()];
            Object[] arguments = new Object[args.size()];
            for (int i = 0; i < args.size(); i++) {
                Object arg = args.get(i);
                if (arg instanceof NativeObject) {
                    NativeObject no = (NativeObject) arg;
                    if (no.isNull()) {
                        partypes[i] = Object.class;
                    } else {
                        partypes[i] = no.getInnerClass();
                    }
                    arguments[i] = no.getValue();
                } else {
                    partypes[i] = arg.getClass();
                    arguments[i] = arg;
                }
            }
            try {
                Method method =
                    MethodUtils.getMatchingAccessibleMethod(this.klass,
                        this.method, partypes);
                if (method == null) {
                    throw new IllegalArgumentException(
                        String.format(
                            "Method '%s' does not exist for %s with the supplied arguments.",
                            this.method, this.klass.getName()));
                }
                Object result = method.invoke(this.object, arguments);
                return result;
                //return new NativeObject(result);
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(String.format(
                    "Could not access method '%s'. Illegal access.",
                    this.method));
            } catch (InvocationTargetException e) {
                throw new IllegalArgumentException(e.getTargetException());
            }
        }

        @Override
        public String getName() {
            // TODO wimthys: what is this used for?
            return "return";
        }

        @Override
        public int getArity() {
            return 0;
        }

        @Override
        public Algebra<?> getAlgebra() {
            return JavaNativeAlgebra.instance;
        }

        @Override
        public Algebra<?> getResultAlgebra() {
            return JavaNativeAlgebra.instance;
        }

    }

    private NativeObject g2n(Object a) {
        return new NativeObject(a);
    }

    @Override
    public NativeObject s2n(String a) {
        return g2n(a);
    }

    @Override
    public NativeObject i2n(Integer a) {
        return g2n(a);
    }

    @Override
    public NativeObject r2n(Double a) {
        return g2n(a);
    }

    @Override
    public NativeObject b2n(Boolean a) {
        return g2n(a);
    }
}
