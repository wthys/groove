/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.algebra;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Operator to handle method calls. Uses the Native signature.
 * @author Wim Thys
 * @version $Revision $
 */
public class MethodCallOperator implements Operator {

    private String methodName = null;
    private Class<?> methodClass = null;
    private Object methodObject = null;

    /**
     * Creates an initialised method call operator.
     * @param methodClass The full name of the class, including package name. 
     * @param methodName The name of the method.
     * @throws IllegalArgumentException when the specified class or method does 
     * not exist. 
     */
    public MethodCallOperator(String methodName, String methodClass)
        throws IllegalArgumentException {
        this.methodClass = loadClass(methodClass);
        if (!classHasMethod(this.methodClass, methodName)) {
            throw new IllegalArgumentException(String.format(
                "Class '%s' does not have a method named '%s'", methodClass,
                methodName));
        }
        this.methodName = methodName;
        this.methodObject = null;
    }

    /**
     * Creates an initialised method call operator.
     * @param object The object to call the method on.
     * @param methodName The name of the method.
     * @throws IllegalArgumentException when the supplied object does not have the specified method.
     */
    public MethodCallOperator(Object object, String methodName)
        throws IllegalArgumentException {
        this.methodClass = object.getClass();
        if (!classHasMethod(this.methodClass, methodName)) {
            throw new IllegalArgumentException(String.format(
                "The supplied object does not have a method named '%s'",
                methodName));
        }
        this.methodName = methodName;
        this.methodObject = object;
    }

    /**
     * Create an initialized method call operator.
     * @param klass The class to use.
     * @param methodName The name of the method to call.
     * @throws IllegalArgumentException when the supplied class does not have the specified method.
     */
    public MethodCallOperator(Class<?> klass, String methodName)
        throws IllegalArgumentException {
        if (!classHasMethod(klass, methodName)) {
            throw new IllegalArgumentException(String.format(
                "Class %s does not have a method named %s", klass.getName(),
                methodName));
        }
        this.methodClass = klass;
        this.methodName = methodName;
        this.methodObject = null;
    }

    /**
     * Creates an empty method call operator. It needs to be further initialised
     * using {@link #setMethodClass(String)} and {@link #setMethodName(String)}.
     */
    public MethodCallOperator() {
    }

    /**
     * Load a class by its name
     * @param className the name of the class, including package.
     * @return The <code>Class<?></code> object representing the specified class.
     * @throws IllegalArgumentException when the class cannot be loaded.
     */
    private static Class<?> loadClass(String className)
        throws IllegalArgumentException {
        Class<?> klass = null;
        try {
            klass = ClassLoader.getSystemClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(String.format(
                "Class '%s' does not exist", className));
        }
        return klass;
    }

    /**
     * Whether the class has a specified method, irregardless of parameters.
     * @param klass the <code>Class<?></code> object of the class.
     * @param method the method name to search for.
     * @return Whether the class supplies the method.
     */
    private static boolean classHasMethod(Class<?> klass, String method) {
        for (Method m : klass.getMethods()) {
            if (method.equals(m.getName())) {
                return true;
            }
        }
        return false;
    }

    /** Set the class for the method.
     * @param methodClass the class, with package.
     */
    public void setMethodClass(String methodClass) {
        this.methodClass = loadClass(methodClass);
    }

    /** Set the class for the method.
     * @param klass the class of the method.
     */
    public void setMethodClass(Class<?> klass) {
        this.methodClass = klass;
    }

    /** Set the object where the method should be called on.
     * @param object the object on which to call the method.
     */
    public void setMethodObject(Object object) {
        this.methodObject = object;
        this.methodClass = object.getClass();
    }

    /** Set the name for the method.
     * @param methodName name of the method, without class.
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /** Whether this operator has a method name.*/
    public boolean hasMethodName() {
        return this.methodName != null;
    }

    /** Whether this operator has a class. */
    public boolean hasMethodClass() {
        return this.methodClass != null;
    }

    /** Whether this operator has an object to act on. */
    public boolean hasMethodObject() {
        return this.methodObject != null;
    }

    /**
     * Whether this operator contains a valid method. Subsequent calls may have 
     * different results because of setting a method name or class.
     * @return If this operator contains a valid method.
     */
    public boolean isValidMethod() {
        if (this.methodName == null || this.methodClass == null) {
            return false;
        } else if (classHasMethod(this.methodClass, this.methodName)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public SignatureKind getSignature() {
        return SignatureKind.NATIVE;
    }

    /**
     * @return The name of the method to be called.
     */
    public String getName() {
        return this.methodName;
    }

    /**
     * @return The class to which the method belongs
     */
    public Class<?> getMethodClass() {
        return this.methodClass;
    }

    @Override
    public int getArity() {
        return 0;
    }

    @Override
    public List<SignatureKind> getParamTypes() {
        //TODO wimthys: only used in {@link AspectNode#inferOutAspects} and {@link Expression#parseAsOperator}.
        /* The first is only used when the edge is an operator, i.e. contains an operator object, which we don't add -> check this!!
         * The second is only used when the operator can be parsed in a 'literal' form, i.e. not the way this can be accessed -> check this too!!
         * If both assumptions are correct, we don't need to care about this method. Until then, don't care about it and just return <code>null</code>.
         */
        return null;
    }

    @Override
    public SignatureKind getResultType() {
        return signatureKindFromReturnType();
    }

    /** 
     * Provide the correct signature for the return type of the enclosed method. 
     * Will compare the signatures to the return type and uses the most specific one.
     * @return The corresponding {@link SignatureKind} of the return type of the 
     *     enclosed method. When in doubt, uses {@link SignatureKind#NATIVE}.
     */
    private SignatureKind signatureKindFromReturnType() {
        //TODO wimthys: we're always in doubt about the return type... for now
        return SignatureKind.NATIVE;
    }

    @Override
    public String getTypedName() {
        return "call:return";
    }

    @Override
    public String getSymbol() {
        return null;
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.UNARY;
    }

    /**
     * @return The object on which to execute the method. <code>null</code> indicates a static method.
     */
    public Object getMethodObject() {
        return this.methodObject;
    }

    /**
     * @return whether the method is static
     */
    public boolean isStaticCall() {
        return (this.methodObject == null && this.methodClass != null);
    }

    @Override
    public String toString() {
        return "" + this.methodClass.getCanonicalName() + "." + this.methodName;
    }

}
