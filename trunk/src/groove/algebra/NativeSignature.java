/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.algebra;

import groove.annotation.Syntax;
import groove.annotation.ToolTipBody;
import groove.annotation.ToolTipHeader;
import groove.java.NativeObject;

/**
 * Interface for native algebras.
 * @author Wim Thys
 * @version $Revision $
 */
@SuppressWarnings("hiding")
public abstract class NativeSignature<Native,String,Bool,Real,Int> implements
        Signature {

    /**
     * See if objects are equal according to <code>a.equals(b)</code>.
     * @param a Object to call <code>equals</code> on.
     * @param b Object to supply to <code>equals</code>.
     * @return The result of <code>a.equals(b)</code>
     */
    @ToolTipHeader("Native equivalence test")
    @Syntax("Q%s.LPAR.a.COMMA.b.RPAR")
    @ToolTipBody("Yields the result of a.equals(b)")
    public abstract Bool eq(Native a, Native b);

    /**
     * See if two native objects are the same object, essentially checking <code>a == b</code>.
     * @param a Native object to compare
     * @param b Native object to compare
     * @return Whether compared objects are the same object.
     */
    @ToolTipHeader("Native identity test")
    @Syntax("Q%s.LPAR.a.COMMA.b.RPAR")
    @ToolTipBody("Yields the result of a == b")
    public abstract Bool same(Native a, Native b);

    /**
     * Perform the standard or overridden {@link Object#toString()} on the argument. Will result in "null" for <code>null</code>.
     * @param a Object to perform {@link Object#toString()} on.
     * @return String representation of the object.
     */
    @ToolTipHeader("Native to string representation")
    @Syntax("Q%s.LPAR.a.RPAR")
    @ToolTipBody("")
    public abstract String toString(Native a);

    /**
     * Convert a native string object to a GROOVE string.
     * @param a Object to convert to a GROOVE string.
     * @return The GROOVE string representation of <code>a</code>
     */
    @ToolTipHeader("Native convert to string")
    @Syntax("Q%s.LPAR.a.RPAR")
    @ToolTipBody("Converts the native to a string")
    public abstract String n2s(Native a);

    /**
     * Convert a native real number object to a GROOVE real number.
     * @param a Object to convert to a GROOVE real number.
     * @return The GROOVE real value of <code>a</code>
     */
    @ToolTipHeader("Native convert to real")
    @Syntax("Q%s.LPAR.a.RPAR")
    @ToolTipBody("Converts the native to a real")
    public abstract Real n2r(Native a);

    /**
     * Convert a native integer object to a GROOVE integer.
     * @param a Object to convert to a GROOVE integer.
     * @return The GROOVE integer value of <code>a</code>
     */
    @ToolTipHeader("Native convert to int")
    @Syntax("Q%s.LPAR.a.RPAR")
    @ToolTipBody("Converts the native to an int")
    public abstract Int n2i(Native a);

    /**
     * Convert a native boolean object to a GROOVE boolean.
     * @param a Object to convert to a boolean.
     * @return The GROOVE boolean value of <code>a</code>
     */
    @ToolTipHeader("Native convert to bool")
    @Syntax("Q%s.LPAR.a.RPAR")
    @ToolTipBody("Converts the native to a bool")
    public abstract Bool n2b(Native a);

    /**
     * Convert a GROOVE string to a native string object;
     * @param a Object to convert to a native.
     * @return The native string object.
     */
    @ToolTipHeader("Convert string to native")
    @Syntax("Q%s.LPAR.a.RPAR")
    @ToolTipBody("Converts the string to a native")
    public abstract Native s2n(String a);

    /**
     * Convert a GROOVE int to a native int object;
     * @param a Object to convert to a native.
     * @return The native int object.
     */
    @ToolTipHeader("Convert int to native")
    @Syntax("Q%s.LPAR.a.RPAR")
    @ToolTipBody("Converts the int to a native")
    public abstract Native i2n(Int a);

    /**
     * Convert a GROOVE real to a native real object;
     * @param a Object to convert to a native.
     * @return The native real object.
     */
    @ToolTipHeader("Convert real to native")
    @Syntax("Q%s.LPAR.a.RPAR")
    @ToolTipBody("Converts the real to a native")
    public abstract Native r2n(Real a);

    /**
     * Convert a GROOVE bool to a native bool object;
     * @param a Object to convert to a native.
     * @return The native bool object.
     */
    @ToolTipHeader("Convert string to native")
    @Syntax("Q%s.LPAR.a.RPAR")
    @ToolTipBody("Converts the bool to a native")
    public abstract Native b2n(Bool a);

    @Override
    final public SignatureKind getKind() {
        return SignatureKind.NATIVE;
    }

    /** This signature does not have literals other than <code>null</code> and NativeObject references in the form of <code>&lt;[number]@[string]&gt;</code>. */
    @Override
    final public boolean isValue(java.lang.String value) {
        boolean isvalue = NativeObject.isRepr(value);
        return isvalue;
    }

    @Override
    public Native getValueFromJava(Object constant) {
        return toValue(constant);
    }

    /**
     * Callback method to convert from the native ({@link Integer})
     * representation to the algebra representation.
     */
    protected abstract Native toValue(Object value);
}
