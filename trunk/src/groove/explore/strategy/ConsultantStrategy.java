/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.explore.strategy;

import groove.explore.result.Acceptor;
import groove.lts.GTS;
import groove.lts.GraphState;

/**
 * Strategy that uses a consultant to influence the transformation.
 * 
 * This strategy should be played with itself as an argument, i.e.
 * <code>ConsultantStrategy constrat = new ConsultantStrategy(aConsultant);
 * constrat.play(constrat.getConsultant());</code> or more conveniently <code>constrat.playConsultant()</code>.
 * 
 * @author wimthys
 * @version $Revision $
 */
public class ConsultantStrategy extends Strategy implements ExploreIterator {

    /** Consultant to use. */
    private final Consultant consultant;

    /**
     * Create a ConsultantStrategy given a Consultant. The Consultant
     * is the main driver of the strategy and should be controlled by
     * an external application.
     * 
     * @param consultant The Consultant to use.
     */
    public ConsultantStrategy(Consultant consultant) {
        this.consultant = consultant;
        super.setAcceptor(this.consultant);
    }

    /**
     * Get the used consultant.
     * @return The consultant used.
     */
    public Consultant getConsultant() {
        return this.consultant;
    }

    /**
     * Convenience method for playing this strategy with it's consultant.
     * 
     * @see groove.explore.strategy.Strategy#play(Halter)
     */
    public void playConsultant() {
        play(getConsultant());
    }

    public void prepare(GTS gts, GraphState state, Acceptor acceptor) {
        this.consultant.prepare(gts, state, acceptor);
    }

    public GraphState doNext() {
        return this.consultant.doNext();
    }

    public boolean hasNext() {
        return this.consultant.hasNext();
    }

    public void finish() {
        this.consultant.finish();
    }
}
