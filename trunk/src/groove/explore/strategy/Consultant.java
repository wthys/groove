/* GROOVE: GRaphs for Object Oriented VErification
 * Copyright 2003--2011 University of Twente
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 *
 * $Id$
 */
package groove.explore.strategy;

import groove.explore.result.Acceptor;
import groove.explore.strategy.Strategy.Halter;
import groove.lts.GTS;
import groove.lts.GraphState;

/**
 * Consultant to interface between the search 
 * strategy and an external controller. 
 * 
 * @author wimthys
 * @version $Revision $
 */
//Inspired by Groove feature request #126
public abstract class Consultant extends Acceptor implements Halter,
        ExploreIterator {

    public abstract void prepare(GTS gts, GraphState state, Acceptor acceptor);

    public abstract GraphState doNext();

    public abstract boolean hasNext();

    public abstract void finish();

    public abstract boolean halt();

}
